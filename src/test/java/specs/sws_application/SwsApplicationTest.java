package specs.sws_application;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.struts.action.ActionForward;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

import eu.specsproject.app.sws.backend.ImplementationInfo;
import eu.specsproject.app.sws.backend.MetricInfo;
import eu.specsproject.app.sws.backend.SQLiteHelper;
import eu.specsproject.app.sws.frontend.rest.REST_Resources;
import eu.specsproject.app.sws.frontend.rest.entities.SLA_TemplateBean;
import eu.specsproject.app.sws.frontend.struts.ImplementAction;
import eu.specsproject.app.sws.frontend.struts.ImplementedAction;
import eu.specsproject.app.sws.frontend.struts.ObserveAction;
import eu.specsproject.app.sws.frontend.struts.SignAction;


public class SwsApplicationTest extends JerseyTest{


    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass Called");

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //do nothing
    }

    @Before
    public void setUpChild() throws Exception {
        System.out.println("setUp Called");
    }

    @After
    public void tearDownChild() throws Exception {
        System.out.println("tearDown Called");
    }

    @Override
    public Application configure() {
        return new ResourceConfig(REST_Resources.class);
    }
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(9998);
    
    @Test
    public final void getSlaTemplateTest() {

        stubFor(get(urlEqualTo("/slo-manager-api/sla-negotiation/sla-templates/SPECS_TEMPLATE_NIST"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody(readFile("src/test/resources/SPECS_TEMPLATE_NIST.xml"))));
        
        final SLA_TemplateBean template = target("/slaTemplate").request().get(SLA_TemplateBean.class);
        Assert.assertNotNull(template);
    }
    
    @Test
    public final void signTest() {
        stubFor(post(urlEqualTo("/sla-manager/cloud-sla/slas/SPECS_TEMPLATE_NIST_1/sign"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("")));
        
        SLA_TemplateBean template = new SLA_TemplateBean();
        template.id = "SPECS_TEMPLATE_NIST_1";
        template.xml = readFile("src/test/resources/SPECS_TEMPLATE_NIST.xml");

        Entity<SLA_TemplateBean> sm = Entity.entity(template, MediaType.APPLICATION_JSON_TYPE); 
        final Response response = target("/sign").request().post(sm);
        System.out.println("Response: "+response);
        Assert.assertEquals(204, response.getStatus());
    }
    
    @Test
    public final void submitTest() {    
        stubFor(put(urlEqualTo("/sla-manager/cloud-sla/slas/SPECS_TEMPLATE_NIST_1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("")));
        
        SLA_TemplateBean template = new SLA_TemplateBean();
        template.id = "SPECS_TEMPLATE_NIST_1";
        template.xml = readFile("src/test/resources/SPECS_TEMPLATE_NIST.xml");

        Entity<SLA_TemplateBean> sm = Entity.entity(template, MediaType.APPLICATION_JSON_TYPE); 
        final Response response = target("/submit").request().post(sm);
        System.out.println("Response: "+response);
        Assert.assertEquals(204, response.getStatus());
    }
    
    @Test
    public final void implementTest() {
        stubFor(post(urlEqualTo("/sla-manager/cloud-sla/slas/1/observe"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("")));
        
        stubFor(get(urlEqualTo("/sla-manager/cloud-sla/slas/1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody(readFile("src/test/resources/SPECS_TEMPLATE_NIST.xml"))));
        
        Entity<String> sm = Entity.entity("1", MediaType.TEXT_PLAIN_TYPE); 
        final Response response = target("/implement").request().post(sm);
        System.out.println("Response: "+response);
        Assert.assertEquals(204, response.getStatus());
    }
    
    @Test
    public final void implementedInfoTest() {
        Entity<String> sm = Entity.entity("1", MediaType.TEXT_PLAIN_TYPE); 
        final Response response = target("/implementedInfo").request().post(sm);
        System.out.println("Response: "+response);
        Assert.assertEquals(200, response.getStatus());
    }
    
    @Test
    public final void implementedMetricValuesTest() {
        Entity<String> sm = Entity.entity("1", MediaType.TEXT_PLAIN_TYPE); 
        final Response response = target("/metricValues").request().post(sm);
        System.out.println("Response: "+response);
        Assert.assertEquals(200, response.getStatus());
    }
    
    @Test
    public final void getSlaOffersTest() {
        stubFor(get(urlEqualTo("/slo-manager-api/sla-negotiation/sla-templates/SPECS_TEMPLATE_NIST"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody(readFile("src/test/resources/SPECS_TEMPLATE_NIST.xml"))));
        
        final SLA_TemplateBean template = target("/slaTemplate").request().get(SLA_TemplateBean.class);
        Assert.assertNotNull(template);
        
        stubFor(post(urlEqualTo("/slo-manager-api/sla-negotiation/sla-templates/SPECS_TEMPLATE_NIST_256/slaoffers"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody(readFile("src/test/resources/SPECS_TEMPLATE_NIST.xml"))));
        
        Entity<String> sm = Entity.entity(readFile("src/test/resources/SPECS_TEMPLATE_NIST.xml"), MediaType.APPLICATION_XML_TYPE); 
        final Response response = target("/slaoffers").request().post(sm);
        Assert.assertEquals(200, response.getStatus());
    }
    
    @Test
    public final void getSlaOfferTest() {
        stubFor(get(urlEqualTo("/slo-manager-api/sla-negotiation/sla-templates/SPECS_TEMPLATE_NIST"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody(readFile("src/test/resources/SPECS_TEMPLATE_NIST.xml"))));
        
        final SLA_TemplateBean template = target("/slaTemplate").request().get(SLA_TemplateBean.class);
        Assert.assertNotNull(template);
        
        stubFor(get(urlEqualTo("/slo-manager-api/sla-negotiation/sla-templates/SPECS_TEMPLATE_NIST_256/slaoffers/10"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody(readFile("src/test/resources/SPECS_TEMPLATE_NIST.xml"))));
        
        Entity<String> sm = Entity.entity("10", MediaType.APPLICATION_XML_TYPE); 
        final Response response = target("/slaOffer").request().post(sm);
        Assert.assertEquals(200, response.getStatus());
    }
    
    
    //REST ENTITIES
    @Test
    public final void collectionTest() {
        eu.specsproject.app.sws.frontend.rest.entities.Collection<String> coll = new eu.specsproject.app.sws.frontend.rest.entities.Collection<String>();
        coll.getItemList();
        coll.setItemList(new ArrayList<String>());
        Assert.assertNotNull(coll);
        Assert.assertNotNull(coll.getItemList());
    }
    
    @Test
    public final void slaTemplaetBeanTest() {
        SLA_TemplateBean bean = new SLA_TemplateBean();
        bean.id = "1";
        bean.xml = "content";
        Assert.assertNotNull(bean);
    }
    
    
    //BACKEND
    @Test
    public final void implamentationInfoTest() {
        ImplementationInfo info = new ImplementationInfo();
        ImplementationInfo infoWithId = new ImplementationInfo("1");

        infoWithId.setChefNodesName(new ArrayList<String>());
        infoWithId.getChefNodesName();
        infoWithId.setInstances(1);
        infoWithId.getInstances();
        infoWithId.setPrivate_IP_Address(new ArrayList<String>());
        infoWithId.getPrivate_IP_Address();
        infoWithId.setPublic_IP_Address(new ArrayList<String>());
        infoWithId.getPublic_IP_Address();
        infoWithId.setSLAID("");
        infoWithId.getSLAID();
        infoWithId.setStatus(infoWithId.getStatus());
        Assert.assertNotNull(info);
        Assert.assertNotNull(infoWithId);
    }
    
    @Test
    public final void metricInfoTest() {
        MetricInfo infoWithId = new MetricInfo();

        infoWithId.setAgreed("");
        infoWithId.getAgreed();
        infoWithId.setCookbook("");
        infoWithId.getCookbook();
        infoWithId.setId("");
        infoWithId.getId();
        infoWithId.setMetricID("");
        infoWithId.getMetricID();
        infoWithId.setName("");
        infoWithId.getName();
        Assert.assertNotNull(infoWithId);
    }
    
    @Test
    public final void sqLiteHelperTest() {
        SQLiteHelper helper = new SQLiteHelper("db_test");
        helper.getMetricValue("idTest", "cookTest", "metricTest", null);
        helper.close();
        Assert.assertNotNull(helper);
    }
    
    //STRUTS ACTIONS
    @Test
    public final void implementActionTest() {
        stubFor(get(urlEqualTo("/sla-manager/cloud-sla/slas/SPECS_TEMPLATE_NIST_1/status"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("")));
        
        ImplementAction action = new ImplementAction();
        try {
            ActionForward forward = action.execute(null, null, null, null);
            Assert.assertNotNull(forward);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public final void implementedActionTest() {
        
        ImplementedAction action = new ImplementedAction();
        try {
            ActionForward forward = action.execute(null, null, null, null);
            Assert.assertNotNull(forward);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public final void observeActionTest() {
        stubFor(get(urlEqualTo("/sla-manager/cloud-sla/slas/SPECS_TEMPLATE_NIST_1/status"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("")));
        
        ObserveAction action = new ObserveAction();
        try {
            ActionForward forward = action.execute(null, null, null, null);
            Assert.assertNotNull(forward);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public final void signActionTest() {
        stubFor(get(urlEqualTo("/sla-manager/cloud-sla/slas/SPECS_TEMPLATE_NIST_1/status"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("")));
        
        SignAction action = new SignAction();
        try {
            ActionForward forward = action.execute(null, null, null, null);
            Assert.assertNotNull(forward);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    private static String readFile(String fileName) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            try {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("\n");
                    line = br.readLine();
                }
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }


}
