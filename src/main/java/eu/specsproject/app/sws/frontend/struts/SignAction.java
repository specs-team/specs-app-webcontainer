/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.app.sws.frontend.struts;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.specsproject.app.sws.backend.SLA_Manager_Client;
import eu.specsproject.app.sws.utility.PropertiesManager;

public class SignAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
                    throws Exception {



        List <String> ids = new ArrayList <String>();

        SLA_Manager_Client client = new SLA_Manager_Client(PropertiesManager.getProperty("sws_conf.properties","sla_manager.endpoint"));
        for (String id :PropertiesManager.getSlaDb()) {


            System.out.println("Id trovato: "+id);
            String state = client.getState(id);

            if (state.equalsIgnoreCase("negotiating"))
                ids.add(id);
        }


        try{
            request.setAttribute("ids", ids);
            return new ActionForward(mapping.getForward());
        }catch(Exception e){
            return new ActionForward();
        }
    }

}
