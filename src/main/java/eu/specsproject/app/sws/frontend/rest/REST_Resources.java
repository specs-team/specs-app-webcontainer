/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.app.sws.frontend.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import eu.specsproject.app.sws.backend.Implementation;
import eu.specsproject.app.sws.backend.ImplementationInfo;
import eu.specsproject.app.sws.backend.MetricInfo;
import eu.specsproject.app.sws.backend.SLA_Manager_Client;
import eu.specsproject.app.sws.backend.SLO_Manager_Client;
import eu.specsproject.app.sws.backend.SQLiteHelper;
import eu.specsproject.app.sws.frontend.rest.entities.SLA_TemplateBean;
import eu.specsproject.app.sws.utility.PropertiesManager;



@Path("/")
public class REST_Resources {
    
    //TODO: pass template id chosen by the user
    private final String masterTemplateId = "SPECS_TEMPLATE_NIST";
    
    private static String templateIdSloManager;
    private static String templateIdSlaManager;

//    @Context ServletContext context;

    @GET
    @Path("/slaTemplate")
    @Produces(MediaType.APPLICATION_JSON)
    public SLA_TemplateBean getTemplate(){
        
        
        //Create a sla on the sla manage
//      SLA_TemplateBean template = new SLA_TemplateBean();
//      SLA_Manager_Client slaClient = new SLA_Manager_Client((String) context.getAttribute("slaManagerEndpoint"));

//      template.id = slaClient.create((String) context.getAttribute("slaTemplate"));

        
        //Get template from slo manager
        System.out.println("SLO MANAGER END PONINT: "+PropertiesManager.getProperty("sws_conf.properties","slo_manager.endpoint"));
        SLO_Manager_Client sloClient = new SLO_Manager_Client(PropertiesManager.getProperty("sws_conf.properties","slo_manager.endpoint"));

        String result = sloClient.getTemplate(masterTemplateId);

        SLA_TemplateBean template = new SLA_TemplateBean();
        templateIdSloManager = result.split("wsag:Name>")[1].substring(0, result.split("wsag:Name>")[1].length()-2);
        templateIdSlaManager = templateIdSloManager.split("_")[templateIdSloManager.split("_").length-1];
        System.out.println("ID TEMPLATE SLO MANAGER: "+templateIdSloManager);
        System.out.println("ID TEMPLATE SLA MANAGER: "+templateIdSlaManager);

        template.id = templateIdSlaManager;
        template.xml = result;
        return template;
        
//      template.xml = (String) context.getAttribute("slaTemplate");    
//      return template;
    }

    
    /*

    @SuppressWarnings("unchecked")
    @GET
    @Path("/brokerTemplates")
    @Produces(MediaType.APPLICATION_JSON)
    public List<BrokerTemplateBean> getBrokerTemplates(){   

        return ( List<BrokerTemplateBean>) context.getAttribute("brokerTemplates"); 
    }
     */




    @POST
    @Path("/sign")
    @Consumes(MediaType.APPLICATION_JSON)
    public void signSLA(SLA_TemplateBean bean ){    

        SLA_Manager_Client client = new SLA_Manager_Client(PropertiesManager.getProperty("sws_conf.properties","sla_manager.endpoint"));

        client.sign(bean);

    }


    @SuppressWarnings("unchecked")
    @POST
    @Path("/submit")
    @Consumes(MediaType.APPLICATION_JSON)
    public void submitSLA(SLA_TemplateBean bean ){  

        SLA_Manager_Client client = new SLA_Manager_Client(PropertiesManager.getProperty("sws_conf.properties","sla_manager.endpoint"));
        client.update(bean);   
        
        PropertiesManager.getSlaDb().add(bean.id);

    }




    @POST
    @Path("/implement")
    @Consumes(MediaType.TEXT_PLAIN)
    public void implementSLA(String slaid){ 

        //TODO: post to planning component path: sla-enforcement/plan-activities
        
        SLA_Manager_Client client = new SLA_Manager_Client(PropertiesManager.getProperty("sws_conf.properties","sla_manager.endpoint"));
        client.observe(slaid);

        //************************
        //XXX
        //FIXME
        int provider=0;//AWS
        //int provider=1;//rackspace
        //************************

//      Implementation.startImplementationNew("plan_example_webpool_sva", (( List<BrokerTemplateBean>) context.getAttribute("brokerTemplates")).get(provider),
//              (String) context.getAttribute("chefServerUsername"),(String)context.getAttribute("chefOrganizationPK"),(String) context.getAttribute("chefEndpoint"),
//              (String)context.getAttribute("chefUserUsername"), (String)context.getAttribute("chefUserPK"),
//              (String)context.getAttribute("hecaEndpoint"), (String)context.getAttribute("hecaPort"));
        Implementation.startImplementation(slaid, 
                client.retrieve(slaid),
                PropertiesManager.getBrokerTemplates().get(provider),
                PropertiesManager.getProperty(PropertiesManager.getProperty("sws_conf.properties", "chef.conf"),"chef.organization"),
                PropertiesManager.getChefOrganizationPK(),
                PropertiesManager.getProperty(PropertiesManager.getProperty("sws_conf.properties", "chef.conf"), "chef.server.endpoint"),
                PropertiesManager.getProperty(PropertiesManager.getProperty("sws_conf.properties", "chef.conf"), "chef.user"),
                PropertiesManager.getChefUserPK(),
                PropertiesManager.getProperty("sws_conf.properties","heca.ip"), 
                PropertiesManager.getProperty("sws_conf.properties","heca.port"));        
    }

    @POST
    @Path("/implementedInfo")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String implementInfo(String slaid){


        //System.out.println(slaid);    
        //ImplementationInfo info = new ImplementationInfo(slaid);      
        ImplementationInfo info = Implementation.getInfo(slaid);

        return new Gson().toJson(info);     
        //return "{\"status\":\"Ready\",\"SLAID\":\"7\", \"instances\":4, \"private_IP_Address\":[\"172.31.33.253\",\"172.31.33.255\",\"172.31.33.252\",\"172.31.33.254\"], \"public_IP_Address\":[\"52.0.78.53\",\"54.164.146.88\",\"54.86.164.60\",\"54.152.233.6\"], \"chefNodesName\":[\"sla-7-172.31.33.253\",\"sla-7-172.31.33.255\",\"sla-7-172.31.33.252\",\"sla-7-172.31.33.254\"], \"recipes\":[ [\"WebPool-haproxy\",\"pippo\"],[\"WebPool-apache\",\"WebPool-apache\"],[\"WebPool-apache\"],[\"WebPool-nginx\"]]}";
    }


    public class MetricAnswer {
        public MetricAnswer() {}
        public String metricID;
        public String newValue;
        public String timestamp;

    }

    @POST
    @Path("/metricValues")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getSLAMetricValues(String slaid){

        if (slaid==null || slaid=="")
            return "";

        List <MetricAnswer> answer = new ArrayList<REST_Resources.MetricAnswer>();
        
        if ( Implementation.getInfo(slaid)==null)
            return "";
        
        Map<String, List<MetricInfo>>  metrics= Implementation.getInfo(slaid).metrics;


        SQLiteHelper helper = new SQLiteHelper( PropertiesManager.getProperty("sws_conf.properties","sqlite.path"));

        for (Map.Entry<String, List <MetricInfo>> entry : metrics.entrySet()) {
            //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

            List <MetricInfo> insideMetrics= entry.getValue();


            for (MetricInfo metric : insideMetrics) {    

                MetricAnswer singleAnswer = new MetricAnswer();
                singleAnswer.metricID = metric.id;
                helper.getMetricValue(slaid, metric.cookbook,metric.metricID , singleAnswer);
                answer.add(singleAnswer);
            
            }
        }



        helper.close();

        //   MetricAnswer ma1 = new MetricAnswer();
        //   ma1.metricID="specs_webpool_M2";
        //   ma1.newValue = "nv 1";
        //   ma1.timestamp = "ts 1";
        //   answer.add(ma1);
        //   
        //   MetricAnswer ma2 = new MetricAnswer();
        //   ma2.metricID="specs_webpool_M2";
        //   ma2.newValue = "nv 2";
        //   ma2.timestamp = "ts 2";
        //   answer.add(ma2);   

        return new Gson().toJson(answer);
    }
    
    
    @POST
    @Path("/slaoffers")
    @Consumes(MediaType.APPLICATION_XML)
    public String slaOffers(String slaCustomTemplate) throws IOException{  
//        slaCustomTemplate = readFile("/Users/Pasquale/git/specs-app-webcontainer/src/main/resources/SLAtemplateDamjan.xml");

        System.out.println("Sla custom Template: "+slaCustomTemplate);

        SLO_Manager_Client sloClient = new SLO_Manager_Client(PropertiesManager.getProperty("sws_conf.properties","slo_manager.endpoint"));
        String response = sloClient.slaOffers(templateIdSloManager, slaCustomTemplate);
        return response;
        
    }
    
    @POST
    @Path("/slaOffer")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.APPLICATION_XML)
    public String getSlaOffer(String slaId){
        System.out.println("Sla offer id request: "+slaId);
        //Get template from slo manager
        SLO_Manager_Client sloClient = new SLO_Manager_Client(PropertiesManager.getProperty("sws_conf.properties","slo_manager.endpoint"));

        String result = sloClient.getSlaOffer(templateIdSloManager, slaId);

        return result;
    }
    
//    private static String readFile(String fileName) throws IOException {
//        BufferedReader br = new BufferedReader(new FileReader(fileName));
//        try {
//            StringBuilder sb = new StringBuilder();
//            String line = br.readLine();
//
//            while (line != null) {
//                sb.append(line);
//                sb.append("\n");
//                line = br.readLine();
//            }
//            return sb.toString();
//        } finally {
//            br.close();
//        }
//    }

}
