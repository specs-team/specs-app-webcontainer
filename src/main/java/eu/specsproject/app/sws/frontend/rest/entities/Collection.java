package eu.specsproject.app.sws.frontend.rest.entities;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlType(propOrder = {"itemList"})
public class Collection<T>{

    @XmlAttribute
    public java.lang.String resource;
    @XmlAttribute
    public Long total;
    @XmlAttribute
    public Long items;

    private List<String> itemList;

    public List<String> getItemList() {
        if (itemList == null)
            itemList = new ArrayList<String>();
        return itemList;
    }

    public void setItemList(List<String> itemList) {
        this.itemList = itemList;
    }
}
