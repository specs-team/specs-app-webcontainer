/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.app.sws.frontend.struts;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.specsproject.app.sws.backend.Implementation;
import eu.specsproject.app.sws.backend.SLA_Manager_Client;
import eu.specsproject.app.sws.utility.PropertiesManager;



@WebServlet(urlPatterns={"/showPlan"})
public class ShowPlanServlet extends HttpServlet {



    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {



        String slaid = req.getParameter("sla_id");

        SLA_Manager_Client client = new SLA_Manager_Client(PropertiesManager.getProperty("sws_conf.properties","sla_manager.endpoint"));
        client.observe(slaid);



        //************************
        //XXX
        //FIXME
        int provider=0;//AWS
        //int provider=1;//rackspace
        //************************

        String plan = Implementation.startImplementation(slaid, 
                client.retrieve(slaid),
                PropertiesManager.getBrokerTemplates().get(provider),
                PropertiesManager.getProperty(PropertiesManager.getProperty("sws_conf.properties", "chef.conf"),"chef.organization"),
                PropertiesManager.getChefOrganizationPK(),
                PropertiesManager.getProperty(PropertiesManager.getProperty("sws_conf.properties", "chef.conf"), "chef.server.endpoint"),
                PropertiesManager.getProperty(PropertiesManager.getProperty("sws_conf.properties", "chef.conf"), "chef.user"),
                PropertiesManager.getChefUserPK(),
                PropertiesManager.getProperty("sws_conf.properties","heca.ip"), 
                PropertiesManager.getProperty("sws_conf.properties","heca.port"));//XXX       

        resp.getWriter().write(plan);

    }

}
