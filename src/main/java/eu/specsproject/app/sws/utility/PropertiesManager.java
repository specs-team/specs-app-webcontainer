package eu.specsproject.app.sws.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodeCredentialsManager;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.datamodel.broker.ProviderCredentialsManager;

public class PropertiesManager {

    public static String getProperty(String filename, String key){
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = PropertiesManager.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                System.out.println("Sorry, unable to find " + filename);
                return "";
            }

            //load a properties file from class path, inside static method
            prop.load(input);

            return prop.getProperty(key);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return "";
    }

    private static HashSet<String> slaDb = new HashSet<String>();

    public static HashSet<String> getSlaDb(){
        return slaDb;
    }

    private static List <BrokerTemplateBean> brokertemplates = new ArrayList<BrokerTemplateBean>();
    public static List <BrokerTemplateBean> getBrokerTemplates(){
        return brokertemplates;
    }

    private static String chefOrganizationPK = "";
    public static String getChefOrganizationPK(){
        return chefOrganizationPK;
    }

    private static String chefUserPK = "";
    public static String getChefUserPK(){
        return chefUserPK;
    }

    static {

        //BROKER PROVIDER CREDENTIALS
        try {
            URL brokerCredentialsDirectoryUrl = Thread.currentThread().getContextClassLoader().getResource(getProperty("sws_conf.properties","broker.provider_credentials.dir"));
            File brokerCredentialsDirectory = new File(brokerCredentialsDirectoryUrl.toURI());

            for ( File f : brokerCredentialsDirectory.listFiles())  {

                Properties providerCredProp = new Properties();
                providerCredProp.load(new FileInputStream(f));

                ProviderCredential cred = new ProviderCredential(providerCredProp.getProperty("username"), 
                        providerCredProp.getProperty("password"));

                ProviderCredentialsManager.add( providerCredProp.getProperty("provider") , cred);
            }          
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //BROKER TEMPLATE
        try {
            URL brokerTemplatesDirectoryUrl = Thread.currentThread().getContextClassLoader().getResource(getProperty("sws_conf.properties","broker.templates.dir"));
            File brokerTemplatesDirectory;
            brokerTemplatesDirectory = new File(brokerTemplatesDirectoryUrl.toURI());

            int counter = 0;


            for ( File f : brokerTemplatesDirectory.listFiles())  {


                byte[] encoded = Files.readAllBytes(Paths.get(f.getAbsolutePath()));            

                BrokerTemplateBean template = BrokerTemplateBean.create(new String(encoded));
                template.setId(String.valueOf(counter));
                brokertemplates.add(template);
                counter++;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //BROKER NODES CREDENTIALS
        try{
            File privateKeyfile = new File(Thread.currentThread().getContextClassLoader().getResource(getProperty("sws_conf.properties","broker.node_credentials.private_key")).toURI());
            File publicKeyfile = new File(Thread.currentThread().getContextClassLoader().getResource(getProperty("sws_conf.properties","broker.node_credentials.public_key")).toURI());

            NodeCredential cred = new NodeCredential(getProperty("sws_conf.properties","broker.node_credentials.rootpassword"), 
                    new String (Files.readAllBytes(Paths.get(publicKeyfile.getAbsolutePath()))), 
                    new String  (Files.readAllBytes(Paths.get(privateKeyfile.getAbsolutePath()))));
            NodeCredentialsManager.add("def", cred);
        }catch(IOException e){
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        //CHEF KEY
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader (Thread.currentThread().getContextClassLoader().getResourceAsStream(PropertiesManager.getProperty(PropertiesManager.getProperty("sws_conf.properties", "chef.conf"),"chef.user.privatekey")))); 
            String line;
            while (( line = br.readLine()) != null) {
                chefUserPK+=(line+"\n");
            }

            br.close();

            br = new BufferedReader(new InputStreamReader ( Thread.currentThread().getContextClassLoader().getResourceAsStream(PropertiesManager.getProperty(PropertiesManager.getProperty("sws_conf.properties", "chef.conf"),"chef.organization.privatekey"))));
            while (( line = br.readLine()) != null) {
                chefOrganizationPK+=(line+"\n");
            }
            br.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }


}
