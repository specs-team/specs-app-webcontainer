/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.app.sws.backend;


import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.gson.Gson;

import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodeCredentialsManager;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.datamodel.broker.ProviderCredentialsManager;
import eu.specs.project.enforcement.broker.ChefService;
import eu.specs.project.enforcement.broker.CloudService;
import eu.specsproject.app.sws.backend.ImplementationInfo.Status;

public class Implementation {

    private static Map <String,ImplementationInfo> implementedSlaInfo = new HashMap<String, ImplementationInfo>();
    private static boolean  debug=true;


//    private static String resolveCookbookName (String capability) {
//
//        switch (capability){
//        case "OSSEC": return "specs-monitoring-ossec";
//        case "WEBPOOL": return "specs-enforcement-webpool";
//        case "OPENVAS": return "specs-monitoring-openvas";
//        }
//
//        return "";
//    }

//    private static String resolveOperationCode (String code) {
//
//
//        switch (code){
//        case "eq": return "=";
//        case "gt": return ">";
//        case "ge": return ">=";
//        case "lt": return "<";
//        case "le": return "<=";
//        }
//
//        return "";
//    }

//    private static String resolveMetric (String metric) {
//
//
//        if (metric.contains("M13"))
//            return "specs.eu/metrics/M13_vulnerability_report_max_age";
//
//        if (metric.contains("M14"))
//            return "specs.eu/metrics/M14_vulnerability_list_max_age";
//
//        if (metric.contains("M16"))
//            return "specs.eu/metrics/M16_dos_report_max_age";
//
//        if (metric.contains("M1"))
//            return "specs.eu/metrics/M1_redundancy";
//
//        if (metric.contains("M2"))
//            return "specs.eu/metrics/M2_diversity";
//
//
//        return "";
//    }


    public static String startImplementation(String slaID, String slaOffer, BrokerTemplateBean bean,
            String organization, String organizationPK, String chefServerEndpoint,String username, String passwordPK, 
            String monitoring_core_ip,String monitoring_core_port) {

        /*
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        Document doc = null;

        ImplementationInfo info = new ImplementationInfo(slaID);

        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(slaOffer.getBytes("utf-8"))));
        } catch (Exception e){

            //TODO nothing.. ?
            e.printStackTrace();
        }

        doc.getDocumentElement().normalize();
        NodeList gTerms = doc.getElementsByTagName("wsag:GuaranteeTerm"); 

        final String WEBPOOL ="WEBPOOL", SVA ="SVA", OPENVAS ="OPENVAS", OSSEC ="OSSEC";
        final String M1 = "specs_webpool_M1", M2 = "specs_webpool_M2", 
                M13_SVA = "specs_sva_M13", M14_SVA = "specs_sva_M14",
                M13_OPENVAS = "specs_openvas_M13",M14_OPENVAS = "specs_openvas_M14",
                M16 = "specs_ossec_M16";



        Set <String> capabilities = new HashSet<String> ();
        Map <String,String> metrics = new HashMap<String,String>();

        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();


        xpath.setNamespaceContext(new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
                if (prefix == null) throw new NullPointerException("Null prefix");
                else if ("specs".equals(prefix)) return "http://specs-project.eu/schemas/sla_related";
                else if ("wsag".equals(prefix)) return "http://schemas.ggf.org/graap/2007/03/ws-agreement";
                else if ("xml".equals(prefix)) return XMLConstants.XML_NS_URI;
                return XMLConstants.NULL_NS_URI;
            }

            // This method isn't necessary for XPath processing.
            public String getPrefix(String uri) {
                throw new UnsupportedOperationException();
            }

            // This method isn't necessary for XPath processing either.
            public Iterator<?> getPrefixes(String uri) {
                throw new UnsupportedOperationException();
            }
        });



        for (int i = 0; i < gTerms.getLength(); i++) {

            Node gterm = gTerms.item(i);

            String capabilityID="";
            String capabilityName = "";
            try {
                capabilityID = ((Node)xpath.evaluate(gterm.getAttributes().getNamedItem("wsag:Name").getNodeValue(),new InputSource(new ByteArrayInputStream(slaOffer.getBytes("utf-8"))), XPathConstants.NODE)).getAttributes().getNamedItem("id").getNodeValue();
                capabilityName = ((Node)xpath.evaluate(gterm.getAttributes().getNamedItem("wsag:Name").getNodeValue(),new InputSource(new ByteArrayInputStream(slaOffer.getBytes("utf-8"))), XPathConstants.NODE)).getAttributes().getNamedItem("name").getNodeValue();
            } catch (DOMException | XPathExpressionException | UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            NodeList slos = ((Element)gterm).getElementsByTagName("specs:SLO");

            List<MetricInfo> metricsInfo = new ArrayList<MetricInfo>();

            for (int j = 0; j < slos.getLength(); j++) {

                Node slo = slos.item(j);

                MetricInfo mInfo = new MetricInfo();

                String key = mInfo.id= slo.getAttributes().getNamedItem("name").getNodeValue();
                mInfo.name= slo.getAttributes().getNamedItem("metric-name").getNodeValue();
                String value =((Element)slo).getElementsByTagName("specs:expression").item(0).getTextContent();
                String operator = ((Element)slo).getElementsByTagName("specs:expression").item(0).getAttributes().getNamedItem("op").getNodeValue();

                mInfo.agreed = resolveOperationCode(operator) + " " + value; 

                metrics.put(key,value);
                metricsInfo.add(mInfo);
                mInfo.cookbook =resolveCookbookName(capabilityID);
                mInfo.metricID=resolveMetric( mInfo.id);
            }

            info.metrics.put(capabilityName, metricsInfo);
            capabilities.add(capabilityID);   
        }



        List<List<String>> recipes=new ArrayList<List<String>> ();


        int machines = 1;
        int diversity =1;

        if (capabilities.contains(WEBPOOL)) {

            if (metrics.containsKey(M1)) 
                try {
                    machines = Integer.valueOf(metrics.get(M1))+1;

                } catch (NumberFormatException e) {
                    machines = 2;
                }
            else
                machines = 2;

            if (metrics.containsKey(M2)){

                try {
                    diversity = Integer.valueOf(metrics.get(M2));

                    if (diversity < 1   || diversity > machines -1  )
                        diversity = 1;
                    else if (diversity > 2)
                        diversity=2;

                } catch (NumberFormatException e) {
                    diversity = 1;
                }
            }

        }

        int modulo=0;

        for (int i  = 0 ; i < machines ; i++){

            List <String> temp = new ArrayList<String>();

            if(capabilities.contains(WEBPOOL)){

                if (i == 0) {
                    temp.add("specs-enforcement-webpool::haproxy"); 
                } else {

                    if (modulo ==0){
                        temp.add("specs-enforcement-webpool::apache");
                        modulo = (modulo+1)%diversity;

                    } else if (modulo ==1) {                
                        temp.add("specs-enforcement-webpool::nginx");
                        modulo = (modulo+1)%diversity;

                    }   
                }
            }



            if (capabilities.contains(OSSEC)){


                if (i == 0) temp.add("specs-monitoring-ossec::server");
                else temp.add("specs-monitoring-ossec::agent");

            }


            if (capabilities.contains(OPENVAS)){

                temp.add("specs-monitoring-openvas::manager");

                if (i == 0) {
                    temp.add("specs-monitoring-openvas::client"); 
                }


            }

            if (capabilities.contains(SVA)){
                temp.add("sva::sva");
            }

            recipes.add(temp);
        }



        String plan = "";

        plan += "machines = " + recipes.size() + "\n";
        for (List<String> recipe : recipes){
            for (String node : recipe ){
                plan +=node+ "\n";
            }
            plan +="------------------------------"+ "\n";
            plan += "\n"; 
        }


        System.out.println(plan);


        //createMachines ( slaID,  bean,   organization,  organizationPK,  chefServerEndpoint, username,  passwordPK, recipes,monitoring_core_ip, monitoring_core_port,info);
        
        return plan;
        */
        return "";
    }


    /*
    private static void createMachines (String slaID, BrokerTemplateBean bean,
            String organization, String organizationPK, String chefServerEndpoint,
            String username, String passwordPK, List<List<String>> recipes,
            String monitoring_core_ip,String monitoring_core_port, ImplementationInfo extraInfo) {

        try {



            ImplementationInfo info = new ImplementationInfo(slaID);
            info.setMetrics(extraInfo.getMetrics());
            implementedSlaInfo.put(slaID, info);

            info.instances=recipes.size();
            info.status=Status.Starting_Implementation;

            if(debug==true){
                java.util.Date date= new java.util.Date();      
                System.out.println(new Timestamp(date.getTime())+" | init impl ");
            }

            String group = "sla-"+slaID;
            NodeCredential nodecred = NodeCredentialsManager.getCredentials("def");


            //---------- To Acquire VMs --------------

            CloudService cloudservice = new CloudService(bean.getProvider(), "root", ProviderCredentialsManager.getCredentials(bean.getProvider()));

            if(debug==true){
                java.util.Date date= new java.util.Date();  
                System.out.println(new Timestamp(date.getTime())+" | VMs creating");    
            }

            NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, recipes.size(), bean, NodeCredentialsManager.getCredentials("def"), 22,80,11211, 9390,1514);
                    
            

            if(debug==true){
                java.util.Date date= new java.util.Date();  
                System.out.println(new Timestamp(date.getTime())+" | VMs created");
            }
            //----------------------------------------- 

            //-------Update Implementation-Info with nodes info----------------
            info.status=Status.VMs_Acquired;
            List<ClusterNode> nodes=nodesInfo.getNodes();

            for(ClusterNode node : nodes){

                String s=node.getPrivateIP();
                info.private_IP_Address.add(s);
                info.public_IP_Address.add(node.getPublicIP());

            }
            //---------------------------------------------------------

            //-----------Preparing VMs : install curl--------------------

            if(debug==true){
                java.util.Date date= new java.util.Date();  
                System.out.println(new Timestamp(date.getTime())+" | VMs preparing");
            }

            Thread[] threads = new Thread[nodes.size()];
            for (int i = 0; i < threads.length; i++) {
                threads[i] = cloudservice.new ExecuteIstructionsOnNode("root",nodes.get(i),prepareEnvironmentScript(""),nodecred.getPrivatekey(),false,cloudservice);
                threads[i].start();
            }

            for (int i = 0; i < threads.length; i++) {
                try {
                    threads[i].join();
                } catch (InterruptedException ignore) {}
            }

            if(debug==true){
                java.util.Date date= new java.util.Date();  
                System.out.println(new Timestamp(date.getTime())+" | VMs prepared");
            }

            info.status=Status.VMs_Prepared;
            //------------------------------------------------------------------


            //------------Bootstrap nodes on chef ----------------------------  
            ChefService chefService = new  ChefService(organization, organizationPK, chefServerEndpoint, username, passwordPK);

            System.out.println("organization: "+organization);
            System.out.println("chefServerEndpoint: "+chefServerEndpoint);
            //create attributes json

            /*
            Plan plan = new Plan();
            VMs vms = new VMs();
            ArrayList<Nodes> myNodes = new ArrayList<Nodes>();

            for (int kk=0; kk<recipes.size();kk++){
                Nodes node = new Nodes();
                node.setNode_id(createIDString()); //random
                ArrayList<Recipes> myRecipes = new ArrayList<Recipes>();

                for (String myStringRecipe : recipes.get(kk)){
                    Recipes recipeObj = new Recipes();
                    String[] splitted = myStringRecipe.split("::");
                    recipeObj.setCookbook(splitted[0]);
                    recipeObj.setName(splitted[1]);
                    myRecipes.add(recipeObj);               
                }
                node.setRecipes(myRecipes);
                node.setPublic_ip(nodesInfo.getNodes().get(kk).getPublicIP());

                ArrayList<String> privateIps = new ArrayList<String>();
                privateIps.add(nodesInfo.getNodes().get(kk).getPrivateIP());
                node.setPrivate_ips(privateIps);
                myNodes.add(node);
            }

            vms.setNodes(myNodes);

            IaaS iaas = new IaaS();
            iaas.setVMs(vms);
            plan.setIaaS(iaas);

            Context context = new Context();
            context.setMonitoring_core_ip(monitoring_core_ip);
            context.setMonitoring_core_port(monitoring_core_port);
            context.setSlaID(slaID);
            context.setPlanID(createIDString()); // tra 1 e X




            plan.setContext(context);

            System.out.println(new Gson().toJson(plan));

            String databagItemID = createIDString();

            chefService.uploadDatabagItem("implementation_plans", databagItemID , new Gson().toJson(plan));


            String attribute= "{\"implementation_plan_id\":\""+databagItemID+"\"}";     

            if(debug==true){
                java.util.Date date= new java.util.Date();     
                System.out.println(new Timestamp(date.getTime())+" | chef node bootstrapping");
            }

            chefService.bootstrapChef(group, nodesInfo, cloudservice,attribute);

            if(debug==true){
                java.util.Date date= new java.util.Date();  
                System.out.println(new Timestamp(date.getTime())+" | chef node bootstrapped");
            }      
            info.status=Status.Chef_Node_Bootstrapped;

            for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());
            //-------------------------------------------------------------------------------------



            //---------------------Executing recipes on nodes ------------------------------------------
            for(int i=0;i<nodes.size();i++ ){

                if(debug==true){
                    java.util.Date date= new java.util.Date();  
                    System.out.println(new Timestamp(date.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(i).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());             
                }
                threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(i), group,nodecred.getPrivatekey(), cloudservice);
                threads[i].start();

                info.recipes.add(InfoRecipes(recipes.get(i)));

            }

            for(int i=0;i<nodes.size();i++ ){
                try {
                    threads[i].join();
                } catch (InterruptedException ignore) {}
            }

            if(debug==true){
                java.util.Date date= new java.util.Date();      
                System.out.println(new Timestamp(date.getTime())+" | recipes completed");
            }

            info.status=Status.Recipes_completed;

            //-------------------------------------------------------------------



            if(debug==true){
                java.util.Date date= new java.util.Date();  
                System.out.println(new Timestamp(date.getTime())+" | fine impl");
            }

            info.status= Status.Ready;
            

        } catch (Exception excp ){
            //TODO check
            excp.printStackTrace();

        }


    }
    */



//    private static String[] prepareEnvironmentScript(String appliance) {
//        //TODO
//        String[] script= {"zypper -n --gpg-auto-import-keys in curl" ,
//                "curl -O http://curl.haxx.se/ca/cacert.pem",
//                "mv cacert.pem /etc/ca-certificates/",
//                "touch .bashrc",
//                "echo 'export CURL_CA_BUNDLE=/etc/ca-certificates/cacert.pem' > .bashrc"
//        };
//        return script;
//    }




    public static ImplementationInfo getInfo(String slaid) {

        return implementedSlaInfo.get(slaid);
    }


//    private static List<String> InfoRecipes(List<String> recipes){
//
//        List<String> ls=new ArrayList<String>();
//        for (String s : recipes){
//
//            ls.add(s.replace("::","-"));
//        }
//
//        return ls;
//    }

//    private static String createIDString(){
//
//        return  UUID.randomUUID().toString().replace("-","");
//
//    }


}
