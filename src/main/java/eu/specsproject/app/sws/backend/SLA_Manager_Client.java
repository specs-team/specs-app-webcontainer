/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.app.sws.backend;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import eu.specsproject.app.sws.frontend.rest.entities.SLA_TemplateBean;

public class SLA_Manager_Client {
    
    String endpoint;
    
    
    public SLA_Manager_Client (String endpoint){
        
        this.endpoint = endpoint;
//        this.endpoint = "http://localhost:8080/sla-manager/cloud-sla/";

    }
    
    
    public String create (String xml) {
        
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(endpoint).path("slas");
         
        String result = target.request(MediaType.TEXT_XML)
            .post(Entity.entity(xml,MediaType.TEXT_XML),String.class);
        
        return result;
    }
    
    
    public void update (SLA_TemplateBean bean) {
        
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(endpoint).path("slas/"+bean.id);
        target.request().put(Entity.entity(bean.xml,MediaType.TEXT_XML),String.class);
    }
    
    
    
    public void sign (SLA_TemplateBean bean) {
        
        Client client = ClientBuilder.newClient();      
        WebTarget target = client.target(endpoint).path("slas/"+bean.id+"/sign");
        target.request().post(Entity.entity(bean.xml,MediaType.TEXT_XML),String.class);
    }
    

    
    public void observe (String id) {
        
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(endpoint).path("slas/"+id+"/observe");
        target.request().post(null);
    }


    public String getState(String id) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(endpoint).path("slas/"+id+"/status");
        return target.request(MediaType.TEXT_XML).get(String.class);
    }


    public String retrieve(String slaid) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(endpoint).path("slas/"+slaid);
        return target.request(MediaType.TEXT_XML).get(String.class);
        
    }
    
    public static void main(String[] args) throws Exception {
        
        //System.out.println(new SLA_Manager_Client("http://localhost:8080/sla_manager_jaxrs/sla_manager_rest_api/").retrieve("1"));
        
    }

    
}
