/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.app.sws.backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import eu.specsproject.app.sws.frontend.rest.REST_Resources.MetricAnswer;

public class SQLiteHelper {

    private final  String query = "SELECT Value, Timestamp FROM Metrics WHERE (Sla_Id=? AND Security_mechanism=? AND Security_metric=?)";
    private final  String driver = "org.sqlite.JDBC";
    
    Connection connection = null;
    
    private  String db_pre_url = "jdbc:sqlite:";

    
    public SQLiteHelper (String db) {
        
        try{
            Class.forName(driver);
            connection = DriverManager.getConnection(db_pre_url +db);
        } catch (Exception e){
            e.printStackTrace();        
        }
        
    }
    
    
    public void close() {

        try{
            if(connection!=null)
                connection.close();
        }catch(SQLException se){
            se.printStackTrace();
        }//end finally try

    }
        
    
    public void getMetricValue ( String slaId, String cookbook, String metric, MetricAnswer ma) {
        System.out.println("SQLiteHelper - getMetricValue");
        PreparedStatement p=null;
        
        try{
            
            p = connection.prepareStatement(query);
            p.setString(1, slaId);
            p.setString(2, cookbook);
            p.setString(3, metric);

            ResultSet rs = p.executeQuery();

            while(rs.next()){
                //Retrieve by column name
                System.out.println("rs.getString(\"Value\") "+rs.getString("Value"));
                System.out.println("rs.getString(\"Timestamp\") "+rs.getString("Timestamp"));
                ma.newValue = rs.getString("Value");
                ma.timestamp = rs.getString("Timestamp");  
            }

            rs.close();
            p.close();
            
            
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(p!=null)
                    p.close();
            }catch(SQLException se2){
            }// nothing we can do
                            
        }//end try
    }
}
