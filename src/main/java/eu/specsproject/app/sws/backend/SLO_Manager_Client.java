/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.app.sws.backend;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.hk2.utilities.reflection.Logger;
import org.slf4j.LoggerFactory;

import eu.specsproject.app.sws.frontend.rest.entities.SLA_TemplateBean;

public class SLO_Manager_Client {
    
    String endpoint;
    
    
    public SLO_Manager_Client (String endpoint){
        
        this.endpoint = endpoint;
//        this.endpoint = "http://localhost:8080/slo-manager-api/sla-negotiation/sla-templates/";
    }
    
    
    public String getTemplates () {
        
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(endpoint);
         
        String result = target.request(MediaType.TEXT_PLAIN).get(String.class);
        
        return result;
    }
    
    public String getTemplate (String templateId) {
        
        Client sloClient = ClientBuilder.newClient();
        WebTarget target = sloClient.target(endpoint).path(templateId);
        String result = target.request().get(String.class);
        
        return result;
    }
    
    public String slaOffers (String templateId, String customSlaTemplate) {
        LoggerFactory.getLogger(SLO_Manager_Client.class).debug("PATH REQUEST OFFERS: "+endpoint+templateId+"/slaoffers");
        LoggerFactory.getLogger(SLO_Manager_Client.class).debug("SLA TEMPLATE: "+customSlaTemplate);
        System.out.println("PATH REQUEST OFFERS: "+endpoint+templateId+"/slaoffers");
        System.out.println("SLA TEMPLATE: "+customSlaTemplate);
        Client sloClient = ClientBuilder.newClient();
        WebTarget target = sloClient.target(endpoint+templateId+"/").path("slaoffers");
        
        String result = target.request().post(Entity.entity(customSlaTemplate, MediaType.TEXT_XML), String.class);

        return result;
    }
    
    public String getSlaOffer (String templateId, String offerId) {
        
        Client sloClient = ClientBuilder.newClient();
        WebTarget target = sloClient.target(endpoint+templateId+"/slaoffers/").path(offerId);
        String result = target.request().get(String.class);
        
        return result;
    }
    
    public static void main(String[] args) throws Exception {
        
        //System.out.println(new SLA_Manager_Client("http://localhost:8080/sla_manager_jaxrs/sla_manager_rest_api/").retrieve("1"));
        
    }

    
}
