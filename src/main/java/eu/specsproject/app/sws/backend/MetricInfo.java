/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.app.sws.backend;

public class MetricInfo {
    
    public String id;
    public String name;
    public String agreed;
    public String cookbook;
    public String getCookbook() {
        return cookbook;
    }
    public void setCookbook(String cookbook) {
        this.cookbook = cookbook;
    }
    public String getMetricID() {
        return metricID;
    }
    public void setMetricID(String metricID) {
        this.metricID = metricID;
    }
    public String metricID;
    
    
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAgreed() {
        return agreed;
    }
    public void setAgreed(String agreed) {
        this.agreed = agreed;
    }
    
    
    
}