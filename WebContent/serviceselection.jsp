<!-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<!-- *** -->
<title>Service Selection</title>
<!-- Bootstrap -->
<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link href="css/bootstrap_wizard/prettify.css" type="text/css"
	rel="stylesheet">
<link href="css/XMLDisplay.css" type="text/css" rel="stylesheet">
<!-- *** -->

<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>
<!-- *** -->
</head>
<script type="text/javascript">
	var title = "Service Selection";
</script>


<!-- **************************************************************************************************************************** -->
<body ng-app="SPECS-APP-WEBCONTAINER">


	<jsp:include page="./navigator.jsp">
		<jsp:param name="_serviceselection" value="class='active'" />
	</jsp:include>


	<div class='container' ng-controller="specsAppWebcontainerContr">

		<section id="wizard">
		<div class="page-header">
			<h1>
				<script language="javascript">
					document.write(title);
				</script>
			</h1>
		</div>

		<!-- SECURITY METRIC PARAMS -->
		<div class="tab-pane" id="tabStoreParams">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Select the Service</h2>
				</div>
				<div class="panel-body ">
					<div class="well">
						Select the type of service that you want to acquire.<br\> The
						needed resources will be acquired from an External CSP. Note that,
						in this version of the application, the selection of the provider
						is not enabled, and the resources will be acquired from Amazon WS.
					</div>

					<div class="panel panel-default">
						<div class="panel-body">
						Valore prelevato: {{testString}}
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- end of rootwizard -->
	</section>
	<!--  end of section -->
	</div>
	<!-- end of container -->
	<jsp:include page="./footer.jsp" />

	<!-- **************************************************************************************************************************** -->
	<!-- **************************************************************************************************************************** -->
	<!-- **************************************************************************************************************************** -->
	<!-- **************************************************************************************************************************** -->

	<!--			<script src="http://code.jquery.com/jquery-latest.js"></script> -->
	<script src="bootstrap/js/jquery.min.js"></script>

	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/bootstrap_wizard/jquery.bootstrap.wizard.js"></script>
	<script src="js/bootstrap_wizard/prettify.js"></script>
	<!-- *************************** -->
	<script src="js/XMLDisplay.js"></script>
	<script src="js/jquery.xpath.js"></script>
	<script src="js/angular.js"></script>
	<!-- *************************** -->
	<script src="js/specs_app.js"></script>
	<!-- *************************** -->

	<!-- Page constructors -->
	<script>
	var testString = "";
	
	var app = angular.module('SPECS-APP-WEBCONTAINER', []);
	
	app.controller('specsAppWebcontainerContr', [ '$scope', function($scope) {
		console.log("chiamata inizializzazione angular");

		$scope.testString = ${testString};
		console.log($scope.testString);

	} ]);
	</script>


</body>
</html>
