<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Observe</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>
<script type="text/javascript" src="js/specs_app.js"></script>
</head>
<!-- ********************************-->

<body>

<jsp:include page="./navigator.jsp">
<jsp:param  name="_obs" value="class='active'"/>
</jsp:include>


<div class="panel panel-default">
		<div class="panel-heading">
			<h2>Observe SLA</h2>
		</div>
		
<div class="panel-body ">

			<div class="well">
In the following, the list of implemented SLAs is reported. <br/> 
For each SLA the current implementation status is displayed:
<ol>
<li><b>Implementation Started:</b> the SLA implementation process has been started; </li>
<li><b>VMs Acquired:</b> the required virtual machines have been acquired from an external CSP;</li>
<li><b>VMs Prepared:</b> each VM has been prepared for the installation of one or more SPECS security mechanisms; </li>
<li><b>Security mechanisms installation started:</b> the security mechanisms installation has been started;</li>
<li><b>Security mechanisms installation completed:</b> all security mechanisms have been installed on acquired VMs;</li>
<li><b>Ready:</b> the acquired resources are ready to use and security mechanisms are running.</li> 
</ol>
The implementation status is updated periodically by the system, unless an instant update is requested by clicking on the "Force Update" button.
When in the "Ready" state, the End-User can request monitoring information by clicking the Monitor Button. 
</div>

	
	<div id="implemented_sla" class="panel panel-default">

 	<div class="panel-heading">
 		<h3>Implemented SLAs</h3>
 	</div>
 
 	<div class="panel-body">
	       
     	<c:choose>	
		<c:when test="${ids.size()!=0}">  
	        
	        <table class="table" id="signed_sla">
	        <thead>
	        <tr>
	        	<th>SLA ID</th>
	        	<th>Implementation Status</th>
	        	<th>Get Monitoring Info</th>
	        </tr>
	        </thead>
	        <tbody>
			        <c:forEach var="slaID" items="${ids}">
			         <tr>
			        	<td><span class="badge">${slaID}</span></td>
			        	<td id="${slaID}_status"></td>
			        	<td> <button disabled id="${slaID}_button" class="btn btn-primary"  onclick="window.location.replace('./implemented.do?slaID=${slaID}');">Monitor</button></td>
			        </tr>
			        
			        
			        </c:forEach>
	        </tbody>
	        
	        </table>

		<h4>
					<b>Last update:</b>
					<span id="update"></span>
					<button class="btn btn-primary" onclick="getInfo()">Force Update</button>
		</h4>
		
	    </c:when>
		
 		<c:otherwise>
			No SLAs were selected for implementation.
		</c:otherwise>
			
	</c:choose>
		
	</div> <!-- end of panel body -->

</div> <!-- end of panel default implemented sla -->
<!-- *****************************************************************  -->
</div> <!-- end of panel body div-->
</div> <!--  panel default  div -->
	 
<jsp:include page="./footer.jsp"/>

<!-- ***************************************** -->

<script src="bootstrap/js/jquery.min.js"></script>
<script src="js/specs_app.js"></script>
<script>
 $(document).ready(function() { 
	 	getInfo();
	 	setInterval(getInfo, 60000);
   });
 
  function getInfo () {
	  
	  $('#signed_sla > tbody  > tr').each(function() {
		  var id = $(this.cells[0]).find('span')[0].innerHTML;
		  makeRequest(id);
	
	  });
	  
	  
  }
  
  
  function translateState (status){
	  
	  switch(status){
		  
	  case "Starting_Implementation" : return "Implementation Started";
	  case "VMs_Acquired" : return "VMs Acquired";
	  case "VMs_Prepared" : return "VMs Prepared";
	  case "Chef_Node_Bootstrapped" : return "Security mechanisms installation started";
	  case "Recipes_completed" : return "Security mechanisms installation completed";
	  case "Ready" : return "Ready";
	  	  
		  
	  }
	  
  }
	  
  function makeRequest (slaID){
	  var xmlhttp;
		  
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	
		xmlhttp.onreadystatechange = function() {
						 if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { 
							 
							 var json = JSON.parse(xmlhttp.responseText);
							 var status =  json.status;
							 
							 $('#'+slaID+'_status').html(translateState(status));
							 
							 if (status=='Ready'){
								 $('#'+slaID+'_button').removeAttr('disabled');
							 }
				
							 $('#update').html( getDateFromLong(new Date().getTime())); 
						 }
		}; 
		 
		xmlhttp.open('POST', 'services/rest/implementedInfo', true);
		xmlhttp.setRequestHeader("Content-type", 'text/plain');
	
		xmlhttp.send(slaID);
 }
 
 </script>


</body>
</html>