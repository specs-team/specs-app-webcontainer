<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Monitoring</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>
<script type="text/javascript" src="js/specs_app.js"></script>
</head>
<!-- ********************************-->

<body>

	<jsp:include page="./navigator.jsp" />

<div class="panel panel-default">
		
		<div class="panel-heading">
			<h2>Implemented SLA Details</h2>
		</div>
		
		
	<div class="panel-body "> <!--  page main content -->

			
			<div class="well">
This page shows the details of an implemented SLA (id and current status) and related monitoring information.<br/>
For each metric that has been selected by the End-User during the negotiation, a panel reports the agreed SLO and the current measured value for the metric. <br/>
Such value is updated periodically by the system, unless an instant update is requested by clicking on the "Force Update" button.
The details of started nodes are shown in an additional hide/show panel at the bottom of the page, which reports the nodes' IDs, IPs and the installed security mechanisms.
<br/>
Note that if no SLOs were selected during negotiation, no metrics will be displayed.
			</div> <!-- end of page main description -->




<div class="panel panel-primary"> <!--  SLA INFO -->

	<div class="panel-heading">
	<h3>SLA Context information</h3>
	</div>			  	
	
	<div class="panel-body">
	   <table>
		<tr><td><h4>SLA ID:</h4></td>        <td> <h4><b><span class="badge">${info.SLAID}</span>  </b> </h4>   </td> </tr>
		<tr><td><h4>SLA Status:</h4> </td>	<td><h4> <b>  ${info.status} </b> </h4>  </td> </tr>
		</table> 
	</div>  		 
					
					 
</div> <!--  end of SLA INFO -->
<!--****************************************************************************************************** -->

<br/>
<br/>

<div id="metric_details"  class="panel panel-default"> <!--  metric TABLE -->
	
	<div class="panel-heading">
	<h3>Metrics Details</h3>
	</div>
	
	<div class="panel-body">
	
				
	 <c:choose>	
		<c:when test="${info.metrics.size()!=0}">
		
					<table class="table">
						<thead>
							<tr>
								<th>Capability</th>
								<th>Metric Name</th>
								<th>SLO</th>
								<th>Measured Value</th>
								<th>Timestamp</th>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="capability" items="${info.metrics}">


								<c:forEach var="metric" items="${capability.value}">
									<tr>
										<td>${capability.key}</td>
										<td>${metric.name}</td>
										<td>${metric.agreed}</td>
										<td id="${metric.id}_value">Not Yet Available</td>
										<td id="${metric.id}_timestamp">---</td>
									</tr>
								</c:forEach>

							</c:forEach>
						</tbody>
					</table>
 
				<h4>
					<b>Last update:</b>
					<span id="update"></span>
					<button class="btn btn-primary" onclick="getInfo()">Force Update</button>
				</h4>
				
	   </c:when>
		
 		<c:otherwise>
			No SLOs were defined in the signed SLA. There are no metrics to monitor.
		</c:otherwise>
			
	</c:choose>
    
    </div> <!--  end of panel body -->

</div> <!-- end of Metric TABLE -->
<!--  *********************************************************************************************** -->


<div  class="panel panel-default"> <!-- NODE DETAILS -->

   <div class="panel-heading">
				<h3>
					Nodes Details
					<button href="#collapse1" class="btn btn-primary nav-toggle">Show Details</button>
				</h3>
  </div>
		
	

<div id="collapse1" style="display: none" class="panel-body">

				 
				<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Node ID</th>
								<th>IPs</th>
								<th>Security mechanisms</th>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="nodeName" items="${info.chefNodesName}" varStatus="status">
							<tr>
								   <td>${status.index+1}</td>
								   
								   <td>${nodeName}</td>
									
								   <td>Public Ip: ${info.public_IP_Address[status.index]}<br />
										Private Ip: ${info.private_IP_Address[status.index]} <br />
								   </td>
									
								 <td>
									<c:forEach var="recipe" items="${info.recipes[status.index]}">
											${recipe}<br />
									</c:forEach>
								 </td>
							
							</tr>
							</c:forEach>

						</tbody>
					</table>
				 
	</div> <!-- fine panel body -->
</div> <!-- end of panel default NODE DETAILS -->
<!-- **************************************************************************** -->


</div> <!--  end of main content  -->
</div> <!--  Div fine pagina -->
<jsp:include page="./footer.jsp" />

			<!-- ***************************************** -->


			<script src="js/angular.js"></script>
			<script src="bootstrap/js/jquery.min.js"></script>

			<script>
				$(document).ready(function() {
					$('.nav-toggle').click(function() {
						//get collapse content selector
						var collapse_content_selector = $(this).attr('href');

						//make the collapse content to be shown or hide
						var toggle_switch = $(this);
						$(collapse_content_selector).toggle(function() {
							if ($(this).css('display') == 'none') {
								//change the button label to be 'Show'
								toggle_switch.html('Show Details');
							} else {
								//change the button label to be 'Hide'
								toggle_switch.html('Hide Details');
							}
						});
					});

				});
			</script>


			<script>
				$(document).ready(function() {
					getInfo();
					setInterval(getInfo, 60000);
				});

				function getInfo() {

					var xmlhttp;

					if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp = new XMLHttpRequest();
					} else {// code for IE6, IE5
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}

					xmlhttp.onreadystatechange = function() {
						if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

							var json = JSON.parse(xmlhttp.responseText);

							for (var i = 0; i < json.length; i++) {
								 
								if( typeof json[i].newValue !== "undefined"){
									$('#' + json[i].metricID + '_value').html(json[i].newValue);
									$('#' + json[i].metricID + '_timestamp').html(getDateFromLong(parseInt(json[i].timestamp)));
								}
							}

							$('#update').html(getDateFromLong(new Date().getTime()));
						}
					};

					xmlhttp.open('POST', 'services/rest/metricValues', true);
					xmlhttp.setRequestHeader("Content-type", 'text/plain');

					xmlhttp.send('${info.SLAID}');
				}
			</script>
</body>
</html>