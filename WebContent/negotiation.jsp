<!-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<!-- *** -->
<title>SPECS Application Negotiation Wizard</title>
<!-- Bootstrap -->
<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link href="css/bootstrap_wizard/prettify.css" type="text/css"
	rel="stylesheet">
<link href="css/XMLDisplay.css" type="text/css" rel="stylesheet">
<!-- *** -->

<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>
<!-- *** -->
</head>
<!-- **************************************************************************************************************************** -->
<body ng-app="SPECS-APP-NEGOTIATION">


	<jsp:include page="./navigator.jsp">
		<jsp:param name="_neg" value="class='active'" />
	</jsp:include>


	<div class='container'>

		<section id="wizard">
		<div class="page-header">
			<h1>SPECS Application Negotiation Wizard</h1>
		</div>

		<div id="rootwizard">
			<div class="navbar">
				<div class="navbar-inner">
					<div class="container">
						<ul>
							<li><a href="#tab1" data-toggle="tab">Start</a></li>
							<li><a href="#tab2" data-toggle="tab">Select Service</a></li>
							<li><a href="#tab3" data-toggle="tab">Select
									Capabilities</a></li>
							<li><a href="#tab4" data-toggle="tab">Select Security
									Controls</a></li>
							<li><a href="#tab5" data-toggle="tab">Define Agreement
									Terms</a></li>
							<li><a href="#tab6" data-toggle="tab">SLA Overview</a></li>
						</ul>
					</div>
				</div>
			</div>



			<div class="tab-content">
				<!-- **************************************************************************************************************************** -->
				<ul class="pager wizard">
					<li class="previous first" style="display: none;"><a href="#">First</a></li>
					<li class="previous"><a href="#">Previous</a></li>
					<li class="next last" style="display: none;"><a href="#">Last</a></li>
					<li class="next"><a href="#">Next</a></li>
				</ul>
				<!-- **************************************************************************************************************************** -->

				<!-- START -->
				<div class="tab-pane" id="tab1">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Start SLA Negotiation</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
								This wizard enables you to negotiate the security level of the
								services you are acquiring. <br /> You will go through several
								steps that will help you select the desired service and the
								security features you are interested in. <br /> At the end of
								the process you will get an SLA that summarizes the guarantees
								that come with your enhanced service.
							</div>
						</div>
					</div>

				</div>
				<!-- end of tab1 -->
				<!-- end of START -->

				<!-- ***************************  -->
				<!-- SDT -->
				<div class="tab-pane" id="tab2">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Service Selection</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
								Select the type of service that you want to acquire.<br\>
								The needed resources will be acquired from an External CSP. Note
								that, in this version of the application, the selection of the
								provider is not enabled, and the resources will be acquired from
								Amazon WS.
							</div>

							<div ng-controller="general as general"
								ng-show="general.metricNAME">Name of metric:
								{{general.metricNAME}}</div>

							<div ng-controller="general as general" ng-show="general.sdts">

								<div class="panel panel-default">
									<div class="panel-body ">

										<table class="table">
											<thead>
												<tr>
													<th>Type of service 12</th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="sdt in general.sdts">
													<td width="15%">{{sdt.name}}</td>
													<td width="20%"><input type="radio" name="SDT_list"
														id="SDT_{{sdt.id}}" value="{{sdt.id}}"></td>
													<td ng-controller="general as general"
														ng-show="general.metricNAME">Name of metric:
														{{general.metricNAME}}</td>
												</tr>

											</tbody>
										</table>
									</div>
								</div>

							</div>
						</div>
					</div>



				</div>
				<!-- end of tab2 -->
				<!-- end of START -->

				<!-- ***************************  -->

				<!-- CAPABILITIES -->
				<div class="tab-pane" id="tab3">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Security Capabilities</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
								Select one or more security capabilities that you want to add to
								the service. <br /> A security capability is a collection of
								security controls, i.e. safeguards and countermeasures, that can
								be applied over your services. <br /> In case you do not select
								any capability, the service will be delivered with no additional
								security features or guarantees.
							</div>


							<table class="table" ng-controller="general as general">

								<!-- <thead ng-show="Object.keys(general.sdts[general.choicedSDT[0]].capabilities).length">  -->
								<thead>
									<tr>
										<th></th>
										<th>Capability Name</th>
										<th>Description</th>
									<tr>
								</thead>
								<tbody>
									<tr
										ng-repeat="(key, cap) in general.sdts[general.choicedSDT[0]].capabilities">
										<td><input type="checkbox" name="capabilityCB"
											id="capability_{{cap.id}}" value="{{cap.id}}" /></td>
										<td width="30%">{{cap.name}}</td>
										<td>{{cap.descr}}</td>
									<tr />
								</tbody>
							</table>

						</div>
					</div>
				</div>
				<!-- end of atab3 -->
				<!-- end of CAPABILITIES -->

				<!-- ***************************  -->

				<!-- Security Controls -->
				<div class="tab-pane" id="tab4">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Security Controls</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
								For each selected capability, choose the security controls you
								are interested in and assign them a score according to the
								importance you give to them. <br /> The listed controls belong
								to specific security frameworks, such as NIST Security Control
								Framework and CSA Cloud Control Matrix. <br /> Each control is
								associated with a set of security metrics, on top of which, in
								the next step, you will be able to define SLOs that will be
								inserted in a Security SLA. <br /> Note that, in case you do
								not select any control, you will not able to define SLOs in the
								next step.
							</div>


							<div ng-controller="general as general">
								<div class="well" ng-show="general.choicedCapabilities.length">

									<div class="panel panel-default"
										ng-repeat="capability in general.choicedCapabilities">
										<div class="panel-heading">
											<h2>{{general.sdts[general.choicedSDT[0]].capabilities[capability.id].name}}</h2>
										</div>
										<div class="panel-body"
											ng-repeat="framework in general.sdts[general.choicedSDT[0]].capabilities[capability.id].frameworks">
											<div class="well">
												<div class="panel panel-default">
													<!-- Default panel contents -->
													<div class="panel-heading">{{framework.name}}</div>

													<table class="table">
														<col width="55%">
														<col width="45%">

														<tr ng-repeat="control in framework.securityControls">
															<td><input type="checkbox" name="SC_CBs"
																id="SC_CB_{{control.id}}"
																value="{{capability.id}}^{{control.id}}" />
																{{control.name}} <br /> Importance weight: <select
																id='SC_IMPORTANCE_{{control.id}}'>
																	<option ng-selected="importance == control.defWeight"
																		ng-repeat="importance in general.importances"
																		value='{{importance}}'>{{importance}}</option>
															</select> <br /></td>
															<td>{{control.descr}}</td>

														</tr>

													</table>


												</div>
											</div>

										</div>
									</div>
								</div>


								<div class="panel panel-primary"
									ng-show="!general.choicedCapabilities.length">
									<div class="panel-body">You did not select any
										capability, therefore no control is displayed.</div>

								</div>


							</div>
							<!--  end of angular general controller -->
						</div>
						<!--  end of panel body -->
					</div>
					<!-- end panel default -->
				</div>
				<!-- end tab4 -->
				<!-- end of Security Controls -->
				<!-- ***************************  -->

				<!-- Metrics and agreement -->
				<div class="tab-pane" id="tab5">

					<div class="panel panel-default">

						<div class="panel-heading">
							<h2>Service Level Objectives</h2>
						</div>
						<div class="panel-body ">

							<div class="well">
								Select the security metrics that you want to monitor and
								specify, for each of them, your importance score and the SLO you
								want to achieve. <br\> The SLO is represented by a
								comparison expression over a metric. If you are not a security
								expert just adopt the default values and go ahead with the next
								step. <br\> Note that, in case you do not select any
								metric, you will not be able to obtain any guarantee over
								implemented security features.
							</div>

							<div ng-controller="general as general">
								<!-- ng-show="$('input:checkbox[name=\'SC_CBs\']:checked').length!=0" -->

								<div class="panel panel-default"
									ng-repeat="capability in general.choicedCapabilities"
									ng-show="capability.controls.length">
									<!-- Default panel contents -->
									<div class="panel-heading">
										<h3>{{general.sdts[general.choicedSDT[0]].capabilities[capability.id].name}}</h3>
									</div>
									<div class="panel-body">
										<!-- List group -->
										<ul class="list-group">
											<!-- <li class="list-group-item" ng-repeat="control in capability.controls"> -->

											<li class="list-group-item"
												ng-repeat="metric in capability.metrics"><input
												type="checkbox" name="METRICS_CBs"
												id="METRICS_CB_{{metric.id}}" value="{{metric.id}}" /><b>{{metric.name}}</b>
												<br /> <i>{{metric.descr}}</i><br /> Importance: <select
												id='SLO_IMPORTANCE_{{metric.id}}'>
													<option ng-selected="importance == metric.importance"
														ng-repeat="importance in general.importances"
														value='{{importance}}'>{{importance}}</option>
											</select> <br /> Expression: <select id="SLO_EXP_OP_{{metric.id}}"
												ng-init="metric.op">
													<option ng-selected="operator.value == metric.op"
														ng-repeat="operator in general.operators"
														value='{{operator.value}}'>{{operator.name}}</option>
											</select> <input type="text" id="SLO_EXP_{{metric.id}}"
												value="{{metric.expression}}" /> {{metric.unit}}<br /></li>

										</ul>

									</div>
								</div>


								<!-- <div class="panel panel-primary" ng-show="!capability.controls.length" >
						   			<div class="panel-body">You did not select any control, therefore no metric is displayed.</div>
								</div> -->


							</div>
							<!--  end of angular controller  -->
						</div>
						<!--  end of panel body -->
					</div>
					<!-- end of panel default -->
				</div>
				<!-- tab5 -->
				<!-- end of Metrics and agreement -->

				<!-- ***************************  -->

				<!-- SHOW SLA -->
				<div class="tab-pane" id="tab6">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Negotiated SLA</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
								A Security SLA has been built according to your choices during
								the negotiation process.<br\> Check it and submit it in
								order to have it stored in your collection of negotiated SLAs.<br\>
								The next steps of the SPECS Application will enable you to sign
								and implement the SLA.
							</div>


							<button class="btn btn-primary"
								onclick="submitSLA(); window.location.replace('./sign.do');">SUBMIT
								AGREEMENT</button>
							<button class="btn btn-primary"
								onclick="window.open('data:application/xml,' + encodeURIComponent(_slaOFFER));">DOWNLOAD
								AGREEMENT</button>
							<div id="SLA-OFFER"></div>
							<button class="btn btn-primary"
								onclick="submitSLA(); window.location.replace('./sign.do');">SUBMIT
								AGREEMENT</button>
							<button class="btn btn-primary"
								onclick="window.open('data:application/xml,' + encodeURIComponent(_slaOFFER));">DOWNLOAD
								AGREEMENT</button>
						</div>
					</div>
				</div>
				<!-- tab6 -->
				<!-- end of SHOW SLA -->


				<!-- **************************************************************************************************************************** -->
				<!-- **************************************************************************************************************************** -->
				<ul class="pager wizard">
					<li class="previous first" style="display: none;"><a href="#">First</a></li>
					<li class="previous"><a href="#">Previous</a></li>
					<li class="next last" style="display: none;"><a href="#">Last</a></li>
					<li class="next"><a href="#">Next</a></li>
				</ul>
			</div>
		</div>
		<!-- end of rootwizard --> </section>
		<!--  end of section -->
	</div>
	<!-- end of container -->
	<jsp:include page="./footer.jsp" />

	<!-- **************************************************************************************************************************** -->
	<!-- **************************************************************************************************************************** -->
	<!-- **************************************************************************************************************************** -->
	<!-- **************************************************************************************************************************** -->

	<!--			<script src="http://code.jquery.com/jquery-latest.js"></script> -->
	<script src="bootstrap/js/jquery.min.js"></script>

	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/bootstrap_wizard/jquery.bootstrap.wizard.js"></script>
	<script src="js/bootstrap_wizard/prettify.js"></script>
	<!-- *************************** -->
	<script src="js/XMLDisplay.js"></script>
	<script src="js/jquery.xpath.js"></script>
	<script src="js/angular.js"></script>
	<!-- *************************** -->
	<script src="js/specs_app.js"></script>
	<!-- *************************** -->

	<!--  ANGULAR -->
	<script>
		var app = angular.module('SPECS-APP-NEGOTIATION', []);

		app.controller('general', function() {
			this.metricNAME = _metricNAME;
			this.sdts = _sdtsList;
			this.choicedSDT = _choicedSDT;
			this.choicedCapabilities = _choicedCapabilities;
			this.metrics = _metrics;
			//////
			this.importances = getWeightConstraints();
			this.operators = getSLOexpOperators();
		});

		var _metricNAME = "";
		var _sdtsList = [];
		var _choicedSDT = [];
		var _choicedCapabilities = [];
		var _metrics = {};
	</script>

	<!-- Page constructors -->
	<script>
		function getWeightConstraints() {
			/*
			//wsag:Item[@wsag:Name='specs:importance_weight']
			 */
			return [ "LOW", "MEDIUM", "HIGH" ];
		}

		function getSLOexpOperators() {
			return [ {
				name : 'equal',
				value : 'eq'
			}, {
				name : 'less than',
				value : 'lt'
			}, {
				name : 'greater than',
				value : 'gt'
			}, {
				name : 'less or equal than',
				value : 'le'
			}, {
				name : 'greater or equal than',
				value : 'ge'
			}

			];
		}

		function parseTemplate() {

			var template = $.parseXML(_slaTEMPLATE);

			//agreement
			var agreementsDOM = $(template).find('GuaranteeTerm');
			var agreements = {};

			for (var n = 0; n < agreementsDOM.size(); n++) {
				var capability = $(agreementsDOM.get(n)).attr('wsag:Name');
				//TODO	
			}

			//slos
			var slosDOM = $(template).find('SLO');
			var slos = {};
			for (var p = 0; p < slosDOM.size(); p++) {

				var variableName = $($(slosDOM.get(p)).find('MetricREF')[0]).text();

				var metricName = $($(slosDOM.get(p)).find('MetricREF')[0]).text();
				var descr = "";
				console.log($($(slosDOM.get(p)).find('SLOexpression')[0]).text());
				var oppp = $($(slosDOM.get(p)).find('SLOexpression')[0]).find('oneOpExpression')[0].getAttribute('operator');
				var exppp = $($(slosDOM.get(p)).find('SLOexpression')[0]).find('oneOpExpression')[0].getAttribute('operand');
				var uu = "";
				var imppp = $($(slosDOM.get(p)).find('importance_weight')[0])
				.text();
				 
				/*
				var variableName = $(slosDOM.get(p)).attr('name');
				var metricName = $(slosDOM.get(p)).attr('metric-name');

				var exppp = $($(slosDOM.get(p)).find('expression')[0]).text();
				var descr = $($(slosDOM.get(p)).find('description')[0]).text();
				var imppp = $($(slosDOM.get(p)).find('importance_weight')[0])
						.text();
				var oppp = $(slosDOM.get(p)).find('expression')[0]
						.getAttribute('op');
				var uu = $(slosDOM.get(p)).find('expression')[0]
						.getAttribute('unit');
				*/
				
				slos[variableName] = {
					variable : variableName,
					metricName : metricName,
					description : descr,
					expression : exppp,
					importance : imppp,
					operator : oppp,
					unit : uu
				};
			}

			//metrics
			var serviceProperties = $(template).find('ServiceProperties');

			for (var kk = 0; kk < serviceProperties.size(); kk++) {

				var variablesForProperty = $(serviceProperties[kk]).find(
						'VariableSet').find('Variable');

				//retrieve capability id using the attribute wsag:name of the service propertty. wsga name is a XPATH to the capability tag
				//XXX the result of the query should be always 1 element, maybe do a check...
				var variablesCapabilityID = $(template).xpath(
						serviceProperties.get(kk).getAttribute('wsag:Name'),
						resolveXpathPrefixes)[0].getAttribute('id');

				var metrics = [];

				for (var jj = 0; jj < variablesForProperty.size(); jj++) {

					var locationXpath = $(
							$(variablesForProperty.get(jj)).find('Location')[0])
							.text();
					var securityControlID = $(template).xpath(locationXpath,
							resolveXpathPrefixes);
					var varName = variablesForProperty.get(jj).getAttribute(
							'wsag:Name');

					var controls = [];
					for (var controlIDindex = 0; controlIDindex < securityControlID
							.size(); controlIDindex++) {

						controls[controlIDindex] = securityControlID[controlIDindex]
								.getAttribute('id');

					}

					//alert (oppp);

					metrics.push({
						id : varName,
						name : slos[varName].metricName,
						descr : slos[varName].description,
						securityControls : controls,
						metricDefinitionXPath : variablesForProperty.get(jj)
								.getAttribute('wsag:Metric'),
						importance : (slos[varName] ? slos[varName].importance
								: null),
						expression : (slos[varName] ? slos[varName].expression
								: null),
						op : (slos[varName] ? slos[varName].operator : null),
						unit : (slos[varName] ? slos[varName].unit : '')
					});

					//alert(variablesCapabilityID + ' ->  ' + variablesForProperty.get(jj).getAttribute ('wsag:Name'));

				}

				//alert(variablesCapabilityID);
				_metrics[variablesCapabilityID] = metrics;

			}

			/*		var allVariables = $(template).find('ServiceProperties').find('VariableSet').find('Variable');
			
			
			
			 for(var i = 0; i < allVariables.size(); i++){
			
			 var locationXpath = $($(allVariables.get(i)).find('Location')[0]).text();
			 var securityControlID = $(template).xpath(locationXpath, resolveXpathPrefixes);
			
			 //alert(allVariables.get(i).getAttribute ('wsag:Name') + " ---> " + securityControlID.size());
			
			
			 var varName =allVariables.get(i).getAttribute ('wsag:Name');
			
			 for (var controlIDindex=0; controlIDindex < securityControlID.size() ; controlIDindex++) {
			
			 var cID = securityControlID[controlIDindex].getAttribute('id');
			 //alert(cID);
			
			
			 _metrics[cID]={
			 name: varName,
			 securityControlXpath: locationXpath ,
			 metricDefinitionXPath: allVariables.get(i).getAttribute('wsag:Metric'),
			 importance: (slos[varName] ? slos[varName].importance : null),
			 expression: (slos[varName] ? slos[varName].expression : null)
			 };
			
			 }
			
			
			 }//for metrics (variables...)
			
			 */
			var sdts = $(template).find("ServiceDescriptionTerm");
			for (var m = 0; m < sdts.size(); m++) {

				var capabilitiesDom = $(sdts.get(m)).find('capability');
				var capabilities = {};

				for (var i = 0; i < capabilitiesDom.size(); i++) {

					var capability = $(capabilitiesDom.get(i));
					var frameworksDom = $(capability).find('controlFramework');
					var frameworks = {};

					for (var j = 0; j < frameworksDom.size(); j++) {

						var controls = {};
						var controlsDom = $(frameworksDom.get(j)).find(
								'securityControl');

						for (var k = 0; k < controlsDom.size(); k++) {

							var controlID = controlsDom.get(k).getAttribute(
									'id');

							controls[controlID] = {
								id : controlID,
								name : controlsDom.get(k).getAttribute('name'),
								descr : $(
										$(controlsDom.get(k)).find(
												'description')[0])
										.text(),
								defWeight : $(
										$(controlsDom.get(k)).find(
												'importance_weight')[0]).text()
							};

						}

						frameworks[frameworksDom.get(j).getAttribute('id')] = {
							id : frameworksDom.get(j).getAttribute('id'),
							name : frameworksDom.get(j).getAttribute(
									'frameworkName'),
							//descr : frameworksDom.get(j).getAttribute('descr'),
							securityControls : controls,
						};

					}

					capabilities[capability.attr('id')] = {
						id : capability.attr('id'),
						name : capability.attr('name'),
						descr : capability.attr('description'),
						frameworks : frameworks,
					};

				}

				var serviceName = sdts.get(m).getAttribute("wsag:ServiceName");
				var sdtName = sdts.get(m).getAttribute("wsag:Name");

				var sdt = {
					id : m,
					service : serviceName,
					name : sdtName,
					capabilities : capabilities
				};

				if (!_services[serviceName]) {
					_services[serviceName] = [];
				}

				_services[serviceName][sdtName] = sdt;
			}
		}

		function createSDTlist() {
			//empty list
			_metricNAME = "afterValue";

			_sdtsList.length = 0;

			var sdts = _services['SecureWebServer'];
			for ( var k in sdts) {
				_sdtsList.push(sdts[k]);
			}
		}

		function createCapabilities() {
			_choicedSDT[0] = $('input[name="SDT_list"]:checked').val();
		}

		function createSecurityControls() {
			//empty previous choices
			_choicedCapabilities.length = 0;

			$("input:checkbox[name='capabilityCB']:checked").each(function() {

				_choicedCapabilities.push({
					id : $(this).val(),
					controls : [],
					metrics : []
				});

			});
		}

		function createAgreement() {

			//empty previous choices
			for (var i = 0; i < _choicedCapabilities.length; i++) {
				_choicedCapabilities[i].controls.length = 0;
				_choicedCapabilities[i].metrics.length = 0;
			}

			$("input:checkbox[name='SC_CBs']:checked").each(function() {

				var splittedVal = $(this).val().split("^");
				var capabilityID = splittedVal[0];
				var controlID = splittedVal[1];

				for (var i = 0; i < _choicedCapabilities.length; i++) {
					if (_choicedCapabilities[i].id == capabilityID) {
						_choicedCapabilities[i].controls.push(controlID);
						break;
					}
				}

			});

			//////////////////////////////////////////////////////////
			for (var i = 0; i < _choicedCapabilities.length; i++) {

				var metrics = _metrics[_choicedCapabilities[i].id];
				_choicedCapabilities[i].metrics = [];

				for (var j = 0; j < metrics.length; j++) {
					var found = false;

					if (!found)
						for (var k = 0; k < _choicedCapabilities[i].controls.length; k++) {

							if (!found)
								for (var l = 0; l < metrics[j].securityControls.length; l++) {

									if (metrics[j].securityControls[l] == _choicedCapabilities[i].controls[k]) {
										found = true;
										_choicedCapabilities[i].metrics
												.push(metrics[j]);
										break;
									}
								}
							else
								break;
						}
					else
						break;

				}

			}
			//////////////////////////////////////////////////////////

		}

		function resolveXpathPrefixes(prefix) {

			

			if (prefix == "specs")
				return "http://specs-project.eu/schemas/SLAtemplate";

			//if (prefix == "specs")
				//return "http://specs-project.eu/schemas/sla_related";

			if (prefix == "wsag")
				return "http://schemas.ggf.org/graap/2007/03/ws-agreement";
			
			if (prefix == "nist")
				return "http://specs-project.eu/schemas/nist";
		}

		function createOffer() {

			var offerDOM = $.parseXML(_slaTEMPLATE);
			$(offerDOM).find('CreationConstraints').remove();

			//SDT
			var SDTs = $(offerDOM).find('ServiceDescriptionTerm');

			/* 	  var _sdtsList=[]; 
				  var _choicedSDT= [];
				  var _choicedCapabilities = [];
				  var _metrics ={}; */

			var sdtName = _sdtsList[_choicedSDT[0]].name;
			for (var i = 0; i < SDTs.length; i++) {

				$(SDTs[i]).find('security_metrics').remove();

				if (SDTs[i].getAttribute('wsag:Name') != sdtName) {
					SDTs[i].remove();
				} else {

					var capabilitiesDOM = $(SDTs[i]).find('capability');

					for (var k = 0; k < capabilitiesDOM.length; k++) {

						var capID = capabilitiesDOM[k].getAttribute('id');
						var foundCap = false;

						for (var j = 0; j < _choicedCapabilities.length; j++) {

							if (_choicedCapabilities[j].id == capID) {

								var secControlsDOM = $(capabilitiesDOM[k])
										.find('securityControl');

								for (var l = 0; l < secControlsDOM.length; l++) {

									var secControlID = secControlsDOM[l]
											.getAttribute('id');
									var foundControl = false;

									for (var m = 0; m < _choicedCapabilities[j].controls.length; m++) {

										if (_choicedCapabilities[j].controls[m] == secControlID) {

											foundControl = true;
											var secValue = $(
													"#SC_IMPORTANCE_"
															+ secControlID)
													.val();

											$(secControlsDOM[l]).text(secValue);
										}

									}

									if (!foundControl) {

										secControlsDOM[l].remove();
									}

								}

								foundCap = true;
							}
						}

						if (!foundCap) {
							capabilitiesDOM[k].remove();
						}
					}
				}

			}

			var gtermsDOM = $(offerDOM).find('GuaranteeTerm');

			for (var k = 0; k < gtermsDOM.length; k++) {

				var gtermName = gtermsDOM[k].getAttribute("wsag:Name");
				if ($(offerDOM).xpath(gtermName, resolveXpathPrefixes).length == 0)
					gtermsDOM[k].remove();

			}

			var slosDOM = $(offerDOM).find('SLO');
			var checkedSLO = {};

			$("input:checkbox[name='METRICS_CBs']:checked").each(function() {

				var sloName = $(this).val();

				checkedSLO[sloName] = {
					id : sloName,
					weight : $("#SLO_IMPORTANCE_" + sloName).val(),
					exp : $("#SLO_EXP_" + sloName).val(),
					op : $("#SLO_EXP_OP_" + sloName).val()
				};

			});

			for (var p = 0; p < slosDOM.size(); p++) {

				var sloName = slosDOM[p].getAttribute('name');
				var foundSLO = false;

				if (checkedSLO[sloName]) {

					var expDOM = $(slosDOM[p]).find('expression');
					var weightDOM = $(slosDOM[p]).find('importance_weight');

					$(expDOM).text(checkedSLO[sloName].exp);
					$(weightDOM).text(checkedSLO[sloName].weight);

					$(expDOM).attr('op', checkedSLO[sloName].op);

					foundSLO = true;

				}

				if (!foundSLO) {

					slosDOM[p].remove();
				}

			}

			_slaOFFER = new XMLSerializer().serializeToString(offerDOM);
			_slaOFFER = _slaOFFER.replace(/<!--(.*?)-->/g, "");
			_slaOFFER = _slaOFFER.replace("wsag:Template ", "wsag:Offer ");
			_slaOFFER = _slaOFFER.replace("wsag:Template>", "wsag:Offer>");
			console.log("SLA OFFER: "+_slaOFFER);
		}

		function checkEmptySlos() {

			var emptySLOs = [];

			$("input:checkbox[name='METRICS_CBs']:checked").each(
					function() {

						var metricID = $(this).val();

						if ($("#SLO_EXP_" + metricID).val() == ''
								|| $("#SLO_EXP_" + metricID).val() == null)
							emptySLOs.push(metricID);
					});

			if (emptySLOs.length > 0) {

				var stringNames = '';

				for (var k = 0; k < emptySLOs.length; k++) {

					stringNames += emptySLOs[k] + ' ';

				}

				alert("Please fill expression field for each selected SLO:    "
						+ stringNames);
				return false;
			}

			return true;
		}
	</script>
	<!-- *************************** -->
	<script>
	<!-- WIZARD -->
		$(document)
				.ready(
						function() {
							$('#rootwizard')
									.bootstrapWizard(
											{
												onNext : function(tab,
														navigation, index) {

													switch (index) {
													case 1:
														getSlaTemplate();
														_services = [];
														parseTemplate();
														createSDTlist();
														angular
																.element(
																		document
																				.getElementById('tab2'))
																.scope()
																.$apply();
														$("#SDT_0").attr(
																'checked',
																'checked');
														break;
													case 2:
														createCapabilities();
														angular
																.element(
																		document
																				.getElementById('tab3'))
																.scope()
																.$apply();
														break;
													case 3:
														createSecurityControls();
														angular
																.element(
																		document
																				.getElementById('tab4'))
																.scope()
																.$apply();
														break;
													case 4:
														createAgreement();
														angular
																.element(
																		document
																				.getElementById('tab5'))
																.scope()
																.$apply();
														break;
													case 5:
														if (!checkEmptySlos()) {
															return false;
														}
														;
														createOffer();
														LoadXMLString(
																'SLA-OFFER',
																_slaOFFER);
														angular
																.element(
																		document
																				.getElementById('tab6'))
																.scope()
																.$apply();
														break;
													/**/
													default: /*do nothing*/
														;
														break;
													}
													/**/
												}, //end of onNext
												onTabShow : function(tab,
														navigation, index) {
													var $total = navigation
															.find('li').length;
													var $current = index + 1;
													var $percent = ($current / $total) * 100;
													$(
															'#rootwizard .progress-bar')
															.css(
																	{
																		width : $percent
																				+ '%'
																	});
													/**/
												}, //end of onTabShow
												onTabClick : function(tab,
														navigation, index) {
													//disable tab clicks
													return false;
													/**/
												} //end of onTabShow
											});
							window.prettyPrint && prettyPrint();
						});
	</script>



</body>
</html>
