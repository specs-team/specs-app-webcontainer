<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Observe</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>
<script type="text/javascript" src="js/specs_app.js"></script>
</head>
<!-- ********************************-->

<body ng-app="observe_app">

<jsp:include page="./navigator.jsp">
<jsp:param  name="_obs" value="class='active'"/>
</jsp:include>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h2>Observe SLA</h2>
		</div>
		<div class="panel-body ">

			<div class="well">The following buttons enable you to observe and monitor an implemented SLA. 
			The SLA IDs are in the grey box. If no button are shown, no signed SLAs are available for implementation
			</div>

			<ul class="list-group">
				<li class="list-group-item"><span class="badge">(slaID)</span><h3>Info</h3></li>
				

				<c:forEach var="slaID" items="${ids}">
					
					<li class="list-group-item" ng-controller="implementation_info">
					
					<span class="badge">${slaID}</span>
						   
						   <div ng-show="implInfo">
						    Implementation Status: {{implInfo.status}}    <br/>
						    Number of instances:   {{implInfo.instances}} <br/>
						    Nodes info: <br/>
							    <div ng-repeat="vvv in implInfo.private_IP_Address">  
							   	  <table class="table">
							   	  	<tr><td>Node {{$index+1}}</td> <td>{{implInfo.chefNodesName[$index]}}</td></tr>
							   	    <tr><td>Public IP</td> <td> {{implInfo.private_IP_Address[$index]}}</td> </tr>
							   	    <tr><td>Private IP </td> <td>  {{implInfo.public_IP_Address[$index]}}</td> </tr>
							   	  </table>
							   	  	Recipes:
							   	 	<div ng-repeat="recipe in implInfo.recipes[$index] track by $index">
									     {{recipe}}    
									</div>
							    </div>
							</div>
		 			 <button class="btn btn-primary" ng-click="myData.doClick('${slaID}', $event)">Update Info</button>
					</li>
				 
				</c:forEach>
			</ul>
		</div>
	</div>
	
	
	<!--  <div  ng-controller="implementation_info">
	 
						   
					<div ng-show="implInfo">
						    Implementation Status: {{implInfo.status}}    <br/>
						    Number of instances:   {{implInfo.instances}} <br/>
						    Nodes info: <br/>
							    <div ng-repeat="vvv in implInfo.private_IP_Address">  
							   	  <table class="table">
							   	  	<tr><td>Node {{$index+1}}</td> <td>{{implInfo.chefNodesName[$index]}}</td></tr>
							   	    <tr><td>Public IP</td> <td> {{implInfo.private_IP_Address[$index]}}</td> </tr>
							   	    <tr><td>Private IP </td> <td>  {{implInfo.public_IP_Address[$index]}}</td> </tr>
							   	  </table>
							   	  	Recipes:
							   	 	<div ng-repeat="recipe in implInfo.recipes[$index] track by $index">
									     {{recipe}}    
									</div>
							    </div>
					</div>
		 			 <button class="btn btn-primary" ng-click="myData.doClick('1', $event)">Update Info</button>
	</div> --> 
	
	 
<jsp:include page="./footer.jsp"/>

<!-- ***************************************** -->


<script src="js/angular.js"></script>
 <script>
    angular.module("observe_app", [])
        .controller("implementation_info", function($scope, $http, $interval) {
            
        	$scope.myData = {};
            $scope.myData.doClick = function(item, event) {
           
            	$http.defaults.headers.common.post = 'text/plain';
                var responsePromise = $http({
                		    url: "services/rest/implementedInfo",
                		    dataType: 'json',
                		    method: 'POST',
                		    data: item,
                		    headers: {
                		    	"Content-Type": "text/plain"
                		    	}
                			});

              responsePromise.success(function(data, status, headers, config) {
                    $scope.implInfo = data;
                });
                responsePromise.error(function(data, status, headers, config) {
                    //do nothing
                	//alert("AJAX failed!");
                });
            };


        } );
  </script>


</body>
</html>