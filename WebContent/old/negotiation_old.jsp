<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<!-- *** -->
<title>SPECS Secure Web Container Application</title>
<!-- Bootstrap -->
<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<link href="css/bootstrap_wizard/prettify.css" type="text/css" rel="stylesheet">
<link href="css/XMLDisplay.css" type="text/css" rel="stylesheet">
<!-- *** -->
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>
<!-- *** -->
</head>
		<!-- **************************************************************************************************************************** -->	
<body>

 
<jsp:include page="./navigator.jsp">
<jsp:param  name="_neg" value="class='active'"/>
</jsp:include>
  

	<div class='container'>

		<section id="wizard">
			<div class="page-header">
				<h1>SPECS Application</h1>
			</div>

			<div id="rootwizard">
				<div class="navbar">
					<div class="navbar-inner">
						<div class="container">
							<ul>
								<li><a href="#tab1" data-toggle="tab">Start</a></li>
								<li><a href="#tab2" data-toggle="tab">Select SDT</a></li>
								<li><a href="#tab3" data-toggle="tab">Select Capabilities</a></li>
								<li><a href="#tab4" data-toggle="tab">Select Security Controls</a></li>
								<li><a href="#tab5" data-toggle="tab">Define Agreement Terms</a></li>
								<li><a href="#tab6" data-toggle="tab">Overview SLA</a></li>
							</ul>
						</div>
					</div>
				</div>
<!-- **************************************************************************************************************************** -->	

				
				<div class="tab-content">
					
<!-- START -->
					<div class="tab-pane" id="tab1"> 
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Start SLA Negotiation</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
							This wizard enable you to Negotiate the security level of the services you are acquiring
							</div>

						</div>
					</div>
				 
						 
					</div>
<!-- end of START -->					
				
												<!-- ***************************  -->
<!-- SDT -->
					<div class="tab-pane" id="tab2"> 
						
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Service Selection</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
							Select the services you aims at having covered by an SLA
							</div>
						</div>
					</div>
						 <br/> 
					  <div id="tab2_SDTlist"></div> 
					</div>
<!-- end of START -->					
				
												<!-- ***************************  -->		
					
<!-- CAPABILITIES -->					
					<div class="tab-pane" id="tab3">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Security Capabilities</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
							Security capabilities are collection of security controls, i.e. safeguards and countermeasures, that can be applied over your services.
							Select the security capabilities you are interested on. We will activate all the services and procedures needed to enforce and monitor the capability you selects. 
							</div>
						</div>
						<table class="table" id="capabilities_table" >
						<thead>
						<tr>
							<td></td>
							<td> Name  </td>
							<td> Description </td>
						<tr>
						<thead>
						<tbody></tbody>
					</table>
						
					</div>
					
					
					
					</div>
<!-- end of CAPABILITIES -->					
					
												<!-- ***************************  -->		
					
<!-- Security Controls -->					
					<div class="tab-pane" id="tab4">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Security Controls</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
							According to the security capabilities chosen, we activate the following security controls. 
							Give an importance to each control according to your requirements.
							If you are not a security expert, just adopt the default values (i.e. skip this ste of the wizard)
							</div>
						</div>
					
						<div id="sec_Controls"></div>
					</div>					
					
					</div>
<!-- end of Security Controls -->		
												<!-- ***************************  -->		
					
<!-- Metrics and agreement -->					
					<div class="tab-pane" id="tab5">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Service Level Objectives</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
							According to the capabilities and controls chosen, we have identified a set of Security metrics that you can monitor in order to verify the respect of the agreement. 
							Please fill in, for each metric, the security level you aims at obtaining and the importance according to your requirement.
							Even in this case, if you are not a security expert, just adopt the default values (i.e. skip this step of the wizard)
							</div>
						</div>
					
					<div id="agreement"></div>
					</div>
					
					</div>
<!-- end of Metrics and agreement -->

												<!-- ***************************  -->		
					
<!-- SHOW SLA -->					
					<div class="tab-pane" id="tab6">
										<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Negotiated SLA</h2>
						</div>
						<div class="panel-body ">
							<div class="well">
							According to the negotiation process, we propose you the following SLA.
							If you accept it, it will be stored in your collection of negotiated SLA.
							In order to make it valid you need to SIGN the SLA.
							In order to make it operative you need to IMPLEMENT the SLA.
							</div>
						</div>
					
					<button class="btn btn-primary" onclick="submitSLA(); window.location.replace('./sign.do');">SUBMIT AGREEMENT</button>
					<div id="SLA-OFFER"></div>
					</div>
					
					
					</div>
<!-- end of SHOW SLA -->


<!-- **************************************************************************************************************************** -->					
<!-- **************************************************************************************************************************** -->					
					<ul class="pager wizard">
						<li class="previous first" style="display: none;"><a href="#">First</a></li>
						<li class="previous"><a href="#">Previous</a></li>
						<li class="next last" style="display: none;"><a href="#">Last</a></li>
						<li class="next"><a href="#">Next</a></li>
					</ul>
				</div>
			</div>

<jsp:include page="./footer.jsp"/>
<!-- **************************************************************************************************************************** -->
<!-- **************************************************************************************************************************** -->	
<!-- **************************************************************************************************************************** -->
<!-- **************************************************************************************************************************** -->	
<!--			<script src="http://code.jquery.com/jquery-latest.js"></script> -->
				<script src="bootstrap/js/jquery.min.js"></script>
			
			<script src="bootstrap/js/bootstrap.min.js"></script>
			<script src="js/bootstrap_wizard/jquery.bootstrap.wizard.js"></script>
			<script src="js/bootstrap_wizard/prettify.js"></script>
						<!-- *************************** -->	
			<script src="js/XMLDisplay.js"></script>
			<script src="js/jquery.xpath.js"></script>
					<!-- *************************** -->	
			<script src="js/specs_app.js"></script>
					<!-- *************************** -->
					
<script> <!-- Page constructors -->

	var templateDOM;
 	var SDTs =[];
	var capabilities;
 
 
//getSlaTemplate($('input[name="_template_select"]:checked').val()); 
 
 function createSDTlist(){
	 
	 $('#tab2_SDTlist').empty();
	 
	var SDTsDOM = $(templateDOM).find("ServiceDescriptionTerm");

		for (var i = 0; i < SDTsDOM.size(); i++) {

			var SDTname = SDTsDOM.get(i).getAttribute("wsag:Name");
			SDTs[i] = $(SDTsDOM.get(i)).find("serviceDescription").find("capabilities")[0];
			
			//GRAPHIC
			$('#tab2_SDTlist').append('<input type="radio" name="sdtList" id="sdtList_'+i+'" value="'+i+'"/>'+ SDTname + '<br/>');
		}

		if (SDTsDOM.size() > 0)
			$("#sdtList_0").attr('checked', 'checked');
	}

	function createCapabilities() {

		$('#capabilities_table tbody').empty();		
	 	capabilities = $(SDTs[$('input[name="sdtList"]:checked').val()]).find('capability');
	 	
		for (var i = 0; i < capabilities.size(); i++) {

			var capabilityName =  capabilities[i].getAttribute('name');
			var capabilityID =  capabilities[i].getAttribute('id');
			var capabilityDescr =  capabilities[i].getAttribute('description');
			
			//GRAPHIC
			var checkbox ='<input type="checkbox" name="capabilityCB" id="capability_'+i+'" value="'+i+'"/>';

			$('#capabilities_table tbody ').append('<tr><td>'+ checkbox +'</td><td>'+ capabilityName + '</td><td>'+ capabilityDescr+ '</td></tr>');

		}

	}


	function createSecurityControls(){
		
		$('#sec_Controls').empty();
			
		$("input:checkbox[name='capabilityCB']:checked").each(function()
				{
					var capability = capabilities[$(this).val()];
					var capabilityName =  capability.getAttribute('name');
					
					//GRAPHIC
					var capabilityFS = '<fieldset><legend>' + capabilityName + '</legend><div id ="capabilityFS_'+capabilityName +'"></div></fieldset>';	
					$('#sec_Controls').append(capabilityFS);
					
					var frameworks = $(capability).find('control_framework');
					
					for (var i = 0 ; i < frameworks.size(); i++){
						
						var framework = frameworks.get(i);
						var frameworkName = framework.getAttribute('frameworkName');
						var frameworkID = framework.getAttribute('id');
						
						
						//GRAPHIC
						var frameworkFS = '<fieldset><legend>' + frameworkName + '</legend><table border="1" id ="frameworkFS_'+frameworkID +'">'+
						'<thead><tr><td></td> <td>Control Name</td> <td>Importance</td></tr></thead><tbody></tbody>'+
						'</table></fieldset>';
					 
						$('#capabilityFS_' +capabilityName).append(frameworkFS);
						
						var securityControls = $(framework).find('security_control');
				
						for (var j = 0 ; j < securityControls.size(); j++){
						
							var securityControl = securityControls.get(j);
							var securityControlId = securityControl.getAttribute('id');
							//TODO create a name for the control
							var securityControlName = securityControl.getAttribute('id');
														
							//GRAPHIC 
							var importance = getImportanceOptions("SC_importance_"+securityControlId);
							var scontrol =
							'<tr>'+
							'<td><input type="checkbox" name="SC_CBs" id="SC_CB_'+ securityControlId +'" value="'+securityControlId+'"/></td>' +
							'<td>'+ securityControlName + '</td>' +
							'<td>' + importance +  '</td>'+
							'<tr/>';
							
							 
							
							$('#frameworkFS_'+frameworkID +' tbody').append(scontrol);
						}						
					}
					
					 
				});
		}
	
	function getImportanceOptions (id) {
		
		//GRAPHIC
		var options = "<select id='"+id+"' >"+
		"<option value='MIN'>min" +
		"<option value='MED'>med" +
		"<option value='HIGH'>high" +
		"</select>";
		
		return options;
		
	}
	
	function createAgreement() {
		
		$('#agreement').empty();
		
		var allVariables = $(templateDOM).find('ServiceProperties').find('VariableSet').find('Variable');
		
		for(var i = 0; i < allVariables.size(); i++){
			
			var locationXpath =   $ ($(allVariables.get(i)).find('Location')[0]).text();
			var secControl = $(templateDOM).xpath(locationXpath, resolveXpathPrefixes);
			var  variableName ="var___Sostituisci";
			
			
			if (secControl.size()==1){
				var secControlId = secControl[0].getAttribute('id');
				var secControlName = secControl[0].getAttribute('id');//TODO change
				
				if($('#SC_CB_'+ secControlId).is(':checked')){
					
					var importance  = getImportanceOptions("METRICS_WEIGHT_"+ variableName);
					
					//graphic
					var metricFS = '<fieldset><legend>' +  
					
					'<input type="checkbox" name="METRICS_CBs" id="METRICS_CB_'+ variableName +'" value="'+variableName+'"/>' +
					secControlName +
					'</legend>' +
					'Importance weight: '+ importance + '<br/>'+
					'Expression: <input type="text"  id="METRICS__'+ variableName +'" /> <br/>' +
					'</fieldset>';
					
	
					
					$('#agreement').append(metricFS);
				}
			}			
		}
		
		
	}
	
	function  resolveXpathPrefixes(prefix) {
	    if (prefix == "specs")
	        return "http://specs-project.eu/schemas/sla_related";
	} 
	
	function createOffer(){
	
		var offerDOM = templateDOM;	
		$(offerDOM).find('CreationConstraints').remove();
		
		//SDT
		var SDTs = $(offerDOM).find('ServiceDescriptionTerm');
		var checkedSDT =	$('input[name=sdtList]:checked').val();
 		
		for (var i=0 ; i < SDTs.length ; i++) {
			
			if (SDTs[i].getAttribute('wsag:Name')!=checkedSDT){
				 
			//	$(offerDOM).find("ServiceDescriptionTerm[wsag:Name$='"+ SDTs[i].getAttribute('wsag:Name') +"']").remove();
				
			}
			
		}  
		
		

		_slaOFFER = new XMLSerializer().serializeToString(offerDOM);
	}
	
</script>										
					<!-- *************************** -->	
<script> <!-- WIZARD -->
	$(document).ready(function() {
	  	$('#rootwizard').bootstrapWizard({onNext: function(tab, navigation, index) {
			
		  		switch(index){
			  		case 1:	getSlaTemplate();
			  				templateDOM = $.parseXML(_slaTEMPLATE);
			  				createSDTlist();
			  				break;
			  		case 2: createCapabilities(); 
			  				break;
			  		case 3: createSecurityControls();
			  				break;
			  		case 4: createAgreement();
	  						break;
			  		case 5: createOffer(); 
			  				LoadXMLString('SLA-OFFER',_slaOFFER);
	  						break;
			  		/**/
			  		default: /*do nothing*/; break;
		  		}
		  	/**/
			}, //end of onNext
			onTabShow: function(tab, navigation, index) {
				var $total = navigation.find('li').length;
				var $current = index+1;
				var $percent = ($current/$total) * 100;
				$('#rootwizard .progress-bar').css({width:$percent+'%'});
			/**/	
			}, //end of onTabShow
			onTabClick: function(tab, navigation, index) {
				//disable tab clicks
				return false;
			/**/
			} //end of onTabShow
	  	});
		window.prettyPrint && prettyPrint();
	});
</script>



</body>
</html>
