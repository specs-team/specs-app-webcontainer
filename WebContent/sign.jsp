<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
           
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SIGN SLA</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

<script type="text/javascript" src="js/specs_app.js"></script>
</head>
<!-- ********************************-->

<body>

<jsp:include page="./navigator.jsp">
<jsp:param  name="_sign" value="class='active'"/>
</jsp:include>

<div class="panel panel-default">
		
		<div class="panel-heading">
			<h2>Sign SLA</h2>
		</div>
		
		<div class="panel-body ">


<div class="well">
This page shows the list of negotiated SLAs. <br/>
Click on the "Sign SLA" button to sign the selected SLA.
</div>

 <!-- ****************************************************************************************** -->       

<div class="panel panel-default">	
	<div class="panel-body">

 	<c:choose>
		<c:when test="${ids.size()!=0}">
				
				<table class="table">
					<thead>
						<tr>
							<th>SLA ID</th>
							<th></th>
						</tr>
					</thead>
					
				
					<tbody>
					<c:forEach var="slaID" items="${ids}">
						<tr>
					
							<td width="20%"><span class="badge">${slaID}</span></td>
							<td><button type="button" class="btn btn-primary"onclick="signSLA(${slaID}); window.location.replace('./implement.do');">Sign SLA</button></td>
							 
						</tr>
					</c:forEach>
					
					</tbody>
				</table>
				
		</c:when>
			
		<c:otherwise>
		There are no SLAs to sign!
		</c:otherwise>
			
	</c:choose>
	
		</div>
</div>			
 
<!-- ****************************************************************************************** -->
</div> <!--  end of main panel body -->
</div> <!--  end of main panel -->
<jsp:include page="./footer.jsp"/>

</body>
</html>