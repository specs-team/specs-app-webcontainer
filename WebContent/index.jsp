<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS DEMO Home</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>

<jsp:include page="./navigator.jsp">
<jsp:param  name="_home" value="class='active'"/>
</jsp:include>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>SPECS Demo Application</h1>
        <p>Welcome to the demonstrator SPECS Application. <br/>
        This wizard will guide you through the main steps of the SLA life cycle, namely:
        <ol> 
        <li>SLA Negotiation</li>
        <li>SLA Implementation</li>
        <li>SLA Monitoring</li>
        </ol>
        <p>Enjoy your SPECS experience!
        </p>
         <p><a class="btn btn-primary btn-large" href="./negotiation_new.jsp" target="_self">Start Negotiation &raquo;</a></p>
      </div>
    </div>


<jsp:include page="./footer.jsp"/>

</body>
</html>