# Web Container App Description #
A web developer, representing the End-User of this user story, aims at acquiring a web container, to run his/her own application, which fulfils some security requirements. The web container is represented by one or more Virtual Machines (VMs) provided by one or more IaaS CSPs. It is reasonable to suppose that the End-User is not an expert in security field: she/he is aware of the technologies that may be involved (SSL, authentication and authorization protocols and so on), but she/he is not aware of the best practices and of how to protect her/his application from malicious attacks. For this reason, the acquisition of VMs and the enforcement of security features are accomplished through SPECS.

## Installation ##

**Install using precompiled binaries**

The precompiled binaries are available under the SPECS Artifact Repository (http://ftp.specs-project.eu/public/artifacts/)

Prerequisites:

* Oracle Java JDK 7;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);
* a running SPECS SLA Platform with Monitoring, Negotiation and Enforcement modules.

Installation steps:

* download the web application archive (war) file from the artifact repository :
http://ftp.specs-project.eu/public/artifacts/applications/secure-web-container/webcontainer-app-0.0.1-SNAPSHOT.war
* the war file has to be deployed in the java servlet/web container

If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.

**Compile and install from source**

The following are the prerequisites to compile and install the Web Container App:

Prerequisites:

* a Git client;
* Apache Maven 3.3.x;
* Oracle Java JDK 7;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);
* a running SPECS SLA Platform with Monitoring, Negotiation and Enforcement modules.

Installation steps:

* clone the Bitbucket repository:
```
#!bash
o	git clone git@bitbucket.org:specs-team/specs-app-webcontainer.git
```
* under specs-app-webcontainer run:
```
#!bash
mvn install
mvn package
```

The installation generates a web application archive (war) file, under the “/target” subfolder. In order to use the component, the war file has to be deployed in the java servlet/web container. If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.


## Usage ##
Once the End-User runs the Web Container application, he/she is provided a simple user interface through which he/she can choose first the capabilities he/she wants to add to the service; for each of them the End-User has to select the security controls and finally he/she has to define the Service Level Objectives he/she wants to be guaranteed. Once the End-User has selected all those information, one or more Service Level Agreement offers are shown to the EU, and he/she has to select and sign one of them. The process just described is the Negotiation phase. 

Then he/she has to implement the signed SLA so that all the components that are necessary to guarantee the signed SLA are deployed on many virtual machines that are acquired from an external service provider. During the implementation process, whose time varies depending on how many resources have to be acquired and on how many components have to be installed, the user is informed about the current status of the implementation phase; once it has finished, the user has the opportunity to get updated information about each metric defined into the SLA: in particular he/she can access to a web page where there is a summary of all those metrics, and for each of them there is the value signed into the SLA and the values measured using SPECS Monitoring System.

The following Activity Diagram describes the steps to execute the main features of the Web Container App

![Activity Diagram.png](https://bitbucket.org/repo/9zGR5z/images/2077178060-Activity%20Diagram.png)

##Web Interface - Example##
Two screenshots of the application are here reported. 

![SCREEN 1.png](https://bitbucket.org/repo/9zGR5z/images/618806757-SCREEN%201.png)

![SCREEN 2.png](https://bitbucket.org/repo/9zGR5z/images/2174936022-SCREEN%202.png)

### Who do I talk to? ###

* Please contact massimiliano.rak@unina2.it
* www.specs-project.eu

SPECS Project 2013 - CeRICT